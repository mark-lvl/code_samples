Assignment description
---

Jane has a basketball game in the center of Tampere at 4:00 pm. To get to the basketball game from Helsinki, she has booked a ticket to the pendolino nr. S45.

Predict if Jane can arrive next weeks Wednesday exactly at 4 pm to the event in Tampere (TPE) with the Pendolino S45 (transfer from the station to the event takes 2 minutes) or should she leave on an earlier train?

Take advantage of historical data for the prediction from the actual arrival times of the trains. Please retrieve data from the Open Data Portal of the Finnish Transport Agency.

1. The Open Data Portal of the Finnish Traffic Agency for railway traffic: https://www.digitraffic.fi/en/railway-traffic/

   A. Railway traffic history: /trains/ in https://rata.digitraffic.fi/swagger/index.html

   B. Example API url for one day's data: https://rata.digitraffic.fi/api/v1/trains/2017-01-01/45

   C. Attached is an obsolete CSV file that you can take advantage of if the API interface causes too much grey hair

2. After you retrieved the data you need, load it to a database

3. Make a forecast of the arrival time of the train to Tampere Station (TPE). As this is not a Data Science assignment, the forecast can be really simple e.g. average or mean.

4. Provide the justification for the forecast you have provided in an easily understandable format, for example through visualizations.

5. Create an application that provides an interface that tells what time the S45 will be next Wednesday at Tampere.