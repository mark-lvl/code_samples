from datetime import (
    datetime, date, timedelta, timezone)

import requests
import pandas as pd

from sqlalchemy import create_engine

#########
# SETTING
#########

DB_ENGINE = create_engine('sqlite:///db_layer.db')

TABLE_NAME = 'train_45_to_tampere'

URL = 'https://rata.digitraffic.fi/api/v1/trains/{}/45'

DATE_RANGE = {
    'start_date': date.today() - timedelta(366),  # get last two years of historical data
    'end_date': date.today() - timedelta(1)  # get train history until yesterday
}

TIMEZONE = 'Europe/Helsinki'

#######
# FUNCS
#######

def return_date_range():
    """Return last 2 years dates"""
    delta = DATE_RANGE.get('end_date') - DATE_RANGE.get('start_date')

    for i in range(delta.days + 1):
        yield DATE_RANGE.get('start_date') + timedelta(days=i)


def utc_to_local(utc_dt):
    """Convert UTC to local time"""
    return pd.to_datetime(utc_dt,
                          infer_datetime_format=True).dt.tz_convert(TIMEZONE)

def call_api(date):
    """Calling the API for collecting historical data on each day"""
    try:
        response = requests.get(URL.format(date))
        if response.json():
            if response.json()[0]['timeTableRows'] is not None:
                return pd.DataFrame(response.json()[0]['timeTableRows'])
    except:
        print('Missing data on {}'.format(str(date)))

def naive_prediction():
    """Naive approach to predict the train ETA"""
    df = pd.read_sql_query('SELECT * FROM {} ORDER BY date'.format(TABLE_NAME),
                           DB_ENGINE)

    print('Arrival time to the basketball game venue by considering'
          ' 2 minutes transfer on next Wendesday: {}'.format(
              datetime.strptime(df['scheduledTime'][0],
                                '%Y-%m-%d %H:%M:%S.%f') + timedelta(minutes=int(2+df['differenceInMinutes'].mean()))
          ))


if __name__ == '__main__':
    print('Please wait to collect the data...\n')
    for date in return_date_range():
        train_date_history = call_api(date)
        
        # Filter the response to only relavant Tampere records
        df_filtered = train_date_history.loc[
            (train_date_history.trainStopping == True) & 
            (train_date_history.stationShortCode == 'TPE') & 
            (train_date_history.type == 'ARRIVAL') & 
            (train_date_history.cancelled == False)][[
                'scheduledTime',
                'actualTime',
                'differenceInMinutes']]
        
        # Add date index to dataframe
        df_filtered.insert(loc=0, column='date', value=date)
        df_filtered.set_index('date', inplace=True)
        
        # Convert datetimes to local time
        df_filtered = df_filtered.apply(lambda x: utc_to_local(x) if x.name in ['scheduledTime', 'actualTime'] else x)
        
        # Dump the result to sqllite database
        df_filtered.to_sql(TABLE_NAME,
                           DB_ENGINE, 
                           if_exists='append', 
                           method='multi')

    # interface for getting an approxiamte time of arrival
    naive_prediction()        

        
