#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
    ================================
    File name: poem.py
    Author: Mark Kaghazgarian
    Date created: 08/03/2019
    Date last modified: 11/03/2019
    Python Version: 3.6
    ================================
    The goal of this module is solving Exercise 
    A.a and A.b

    to run the code and get the manual:
        
        $ python poem.py -h
    
    
    to run the any of the method put the method name 
    in front of -m option:
        
        $ python poem.py -m download
        or
        $ python poem.py -m counter
        or
        $ python poem.py -m waiting
"""

import requests, bs4
import os
import csv
import argparse

PROJ_DIR = os.path.dirname(os.path.abspath(__file__))
BASE_URL = 'http://runeberg.org/kalevala/'
OUTPUT_DIR = 'output'

def get_url(url):
    res = requests.get(url)
    # try to get the result or thrown exception
    try:
        res.raise_for_status()
    except Exception as exc:
        print('There was a problem: %s' % (exc))
    
    return res

def download_poems():
    """Scraping poems from site and saving in disk."""
    # Scraping the home page
    mainPage = get_url(BASE_URL)

    # Converting the home page content to html
    mainPageContent = bs4.BeautifulSoup(mainPage.text, 'html.parser')

    # Filtering all the anchors in the p tag
    poemLinks = mainPageContent.select('p a')

    for p in poemLinks[:-6]:  # last 6 link are not poem and should be excluded
        # Scraping the poem page
        poem = get_url(BASE_URL + p['href'])
        # Converting the poem page content to html
        poemContent = bs4.BeautifulSoup(poem.text, 'html.parser')
        # extracting the peom name, use as file name
        poemName = poemContent.select('h3')
        # check for output directory existance
        if not os.path.exists(os.path.join(PROJ_DIR, OUTPUT_DIR, 'poem_files')):
            os.makedirs(os.path.join(PROJ_DIR, OUTPUT_DIR, 'poem_files'))

        fileName = poemName[0].getText() + '.txt'

        try:
            with open(os.path.join(PROJ_DIR, OUTPUT_DIR, 'poem_files', fileName), "w") as poemFile:
                for poemPart in poemContent.select('p')[:1]:  # poor html structure, the first p contain whole poem text
                    
                    # cleanup the text by removing special characters and lowerazing the text
                    poemText = poemPart.getText().lower()
                    filterChars = '!?;:,."'  # chars to remove  
                    for char in filterChars:
                        poemText = poemText.replace(char,"")
                    
                    # Locate last word in the peom text 
                    endPhrase = poemText.find("project runeberg")
                    # Cut out only the poem text and remove extra texts.
                    # Dumping the poem in the file.
                    poemFile.write(poemText[0:endPhrase])
                    print("SUCCESS: poem saved in " + fileName)
        except IOError:
            print('ERROR: An error occured trying to write the file:' + fileName)
        except:
            print('ERROR: An error occured on processing:' + fileName) 

def word_line_counter(export_csv=True, verbose_mode=True):
    """
    Return the line and word count for each poem.
    
    :param bool export_csv: Export the result in csv format.
    :param bool verbose_mode: Display the result in the terminal.
    """
    
    # placeholder for a list of dicts consists of 
    # conting rows and words on each poem as the key.
    toCSV = []

    try:
        # iterating on poems files and process each.
        files = [file for file in os.listdir(os.path.join(PROJ_DIR, OUTPUT_DIR, 'poem_files')) 
                if file.endswith(".txt")]
    except FileNotFoundError:
        print("Error: no poem file found, run download_peoms method first.")
        return
    
    for file in files:
        with open(os.path.join(PROJ_DIR, OUTPUT_DIR, 'poem_files', file), "r") as fp:
            result = [len(line) for line in fp if line.strip() != '']
            poemName = file.replace(".txt","")
            rowCount = len(result)
            wordCount = sum(result)

            if verbose_mode:
                print("{:25} | Row_count: {} | Word_count: {}".format(poemName, 
                                                                        rowCount,
                                                                        wordCount))
            if export_csv:
                toCSV.append({'poem':poemName,
                              'row_count':rowCount,
                              'word_count': wordCount})
    
    if export_csv:
        try:
            # check for output directory existance
            if not os.path.exists(os.path.join(PROJ_DIR, OUTPUT_DIR, 'data')):
                os.makedirs(os.path.join(PROJ_DIR, OUTPUT_DIR, 'data'))
            # export the list of dicts to csv file
            with open(os.path.join(PROJ_DIR, 
                                OUTPUT_DIR, 
                                'data', 
                                'poem_row_word_count.csv'), 'w') as output_file:
                dict_writer = csv.DictWriter(output_file, fieldnames=toCSV[0].keys())
                dict_writer.writeheader()
                dict_writer.writerows(toCSV)
                print("SUCCESS: csv file was created in output folder.")
        except IOError:
            print('ERROR: An error occured trying to write the file.')
        except Exception as exp:
            print('ERROR: An error occured.' + str(exp))


def word_waiting_time(export_csv=True, verbose_mode=False):
    """
    Count and average "waiting time" for each word within a given poem.
    
    :param bool export_csv: Export the result in csv format.
    :param bool verbose_mode: Display the result in the terminal.
    """

    # placeholder for a list of dicts for conting 
    # occurance, waiting time and position of occurance of a given word.
    toCSV = []

    try:
        # iterating on poems files and process each.
        files = [file for file in os.listdir(os.path.join(PROJ_DIR, OUTPUT_DIR, 'poem_files')) 
                if file.endswith(".txt")]
    except FileNotFoundError:
        print("Error: not poem file found, run download_peoms method first.")
        return
    
    for file in files:
        with open(os.path.join(PROJ_DIR, OUTPUT_DIR, 'poem_files', file), "r") as fp:
            # Reading the whole poem at once
            content = fp.read()
            # Split the poem to words by space
            words = content.split()
            # A placeholder for listing all occurances of a word within the poem
            word_occurance = {}
            poemName = file.replace(".txt","")
            for word in words:
                # For any given word as dictioanry key assing a list of occurances positions
                # in the poem.
                word_occurance[word] = [occ for occ,curr_word in enumerate(words) 
                                        if curr_word.strip() == word]

            for word,occurances in word_occurance.items():
                if len(occurances) == 1:
                    waitingTime = 'Not Defined'
                else:
                    # Calculating average waiting time according to exercise definition.
                    waitingTime = [y-x for x, y in zip(occurances, occurances[1:])]
                    waitingTime = round(sum(waitingTime) / len(waitingTime), 2)
                count = len(occurances)
                if export_csv:
                    toCSV.append({'poem': poemName, 
                                  'word': word, 
                                  'count': count, 
                                  'waiting_time': waitingTime, 
                                  'occurances_at': occurances})
                if verbose_mode:
                    print("{:<25}| Count: {:<2} | Waiting_time: {:<12}| Occ_pos:{}".format(word, 
                                                                                    count, 
                                                                                    waitingTime, 
                                                                                    occurances))
    
    if export_csv:
        try:
            # check for output directory existance
            if not os.path.exists(os.path.join(PROJ_DIR, OUTPUT_DIR, 'data')):
                os.makedirs(os.path.join(PROJ_DIR, OUTPUT_DIR, 'data'))
            # export the list of dicts to csv file
            with open(os.path.join(PROJ_DIR, 
                                OUTPUT_DIR, 
                                'data', 
                                'word_freq.csv'), 'w') as output_file:
                dict_writer = csv.DictWriter(output_file, fieldnames=toCSV[0].keys())
                dict_writer.writeheader()
                dict_writer.writerows(toCSV)
                print("SUCCESS: csv file was created in output folder.")
        except IOError:
            print('ERROR: An error occured trying to write the file.')
        except Exception as exp:
            print('ERROR: An error occured.' + str(exp))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Solver for Exercise A.a and A.b.')
    
    # this list only use for mapping args to actual functional above
    method_maps = {
        'download': 'download_poems',
        'counter': 'word_line_counter',
        'waitingtime': 'word_waiting_time'
    }

    parser.add_argument(
        "-m",
        "--method",
        help="choice the method you want to run form the list",
        choices= ['download', 'counter', 'waiting'],
        required=True)

    parser.add_argument(
        "-v",
        "--verbose", 
        help="print the result in console",
        action="store_true")

    args = parser.parse_args()

    # initiate the function arguments
    verbose = False
    if args.verbose:
        verbose = True
    
    # invoke the approperiate method with parameters
    if args.method:
        if (args.method == 'download'):
            download_poems()    
        elif (args.method == 'counter'):
            word_line_counter(verbose_mode=verbose)
        elif (args.method == 'waiting'):
            word_waiting_time(verbose_mode=verbose)