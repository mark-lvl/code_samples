Assignment description
---

Work on the Kalevala poems from: http://runeberg.org/kalevala/

 a. create functionality to load the poems to text file + possible light cleaning.
 b. for each poem in the kalevala series calculate the following stats:

* For each poem: row count, word count

* For each word: count and average "waiting time" between words
  waiting time: word count between consecutive appearances of the same word within the poem. Not defined, if word appears in the data poem only once.

  Note: Almost similar words can be treated as distinct.

 c. Analyze the data

* visualize distribution of total word and row counts across the 50 poems
* propose and implement a simple heuristic to extract keywords for each poem utilizing the metrics calculated above
      here keyword: word appears in poem X (significant), but is rare in general across all poems
* create a weighted graph + visualization of the poems, where weight is a function of
      shared keywords between poems. i.e. poems with similar keywords should be close to each other
      -- any interesting structure, problems?

