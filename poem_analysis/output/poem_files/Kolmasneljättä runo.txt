kullervo kalervon poika otti konttihin evästä
ajoi lehmät suota myöten itse kangasta kapusi
sanan virkki vieressänsä kertoeli käyessänsä
voi minä poloinen poika voi poika polon-alainen
jo minä johonki jou'uin jou'uin joutavan jälille
härän hännän paimeneksi vasikkojen vaalijaksi
joka suon on sotkijaksi maan pahan matelijaksi

istui maahan mättähälle päätyi päivän rintehesen
siinä virkki virsissänsä lauluissansa noin lateli
paistapa jumalan päivä herran kehrä hellittele
sepon karjan kaitsijalle emännällen ensinkänä
emäntä hyvin elävi vehnäsiä viiltelevi
piirosia pistelevi voita päälle vuolaisevi
paimen parka kuivan leivän kuivan kuoren kurskuttavi
kauraisen kavertelevi lementtisen leikkoavi
olkisen ojentelevi petäisen peiputtavi
veen lepillä liukkoavi märän mättähän nenästä

mene päivä viere vehnä alene jumalan aika
kule päivä kuusikolle viere vehnä vitsikölle
karkoa katajikolle lennä leppien tasalle
päästä paimenta kotihin voivatia vuolemahan
rieskoa repäisemähän kakkaroita kavamahan

silloin ilmarin emäntä paimenen pajattaessa
kullervoisen kukkuessa jo oli vuollut voivatinsa
itse rieskansa reväisnyt kakkaransa kaivaellut
keittänyt vetisen vellin kylmän kaalin kullervolle
jos' oli rakki rasvan syönyt musti murkinan pitänyt
merkki syönyt mielin määrin halli haukannut halunsa

lintunen lehosta lauloi pieni lintu pensahasta
jos oisi aika orjan syöä isottoman illastella
kullervo kalervon poika katsoi pitkän päivän päälle
itse tuon sanoiksi virkki jo nyt on aika atrioia
aika ruoalle ruveta evähiä etsiskellä

ajoi lehmänsä levolle karjan maata kankahalle
itse istui mättähälle vihannalle turpehelle
laski laukkunsa selästä otti leivän laukustansa
katselevi kääntelevi tuosta tuon sanoiksi virkki
moni on kakku päältä kaunis kuorelta kovin sileä
vaan on silkkoa sisässä akanoita alla kuoren

veti veitsensä tupesta leivän leikkaellaksensa
veitsi vierähti kivehen kasahutti kalliohon
terä vieri veitsosesta katkesi kurauksuesta

kullervo kalervon poika katselevi veitsyttänsä
itse päätyi itkemähän sanan virkkoi noin nimesi
yks' oli veitsi veikkoutta yksi rauta rakkautta
isän saamoa eloa vanhemman varustamata
senki katkaisin kivehen karahutin kalliohon
leipähän pahan emännän pahan vaimon paistamahan

millä nyt maksan naisen naurun naisen naurun piian pilkan
akan ilkeän evähät pahan porton paistannaiset

varis vaakkui varvikosta varis vaakkui korppi koikkui
oi on kurja kullansolki ainoa kalervon poika
mit' olet mielellä pahalla syämellä synkeällä
ota vitsa viiakosta koivu korven notkelmosta
aja suolle sontareiet lehmät liejuhun levitä
puolen suurille susille toisen korven kontioille

kaikoa suet kokohon karhut kaikki katrahasen
suet pistä pienikiksi karhut kyytäksi kyhäise
aja karjana kotihin kirjavana kartanolle
sillä maksat naisen naurun pahan vaimon parjaukset

kullervo kalervon poika itse tuon sanoiksi virkki
malta malta hiien huora jos itken isoni veistä
vielä itkenet itseki itket lypsylehmiäsi

otti vitsan viiakosta katajaisen karjanruoskan
sorti suohon lehmäkarjan härät murtohon murenti
puoliksi susien syöä puolen korven kontioille
suet lausui lehmäsiksi karhut karjaksi rakenti
minkä pisti pienikiksi kunka kyytäksi kyhäisi

lonkui päivä lounahasen kiertyi keski-illoillensa
kulki kuusikon tasalle lenti lehmäslypsykselle
tuo pahainen paimen raiska kullervo kalervon poika
ajoi kontiot kotihin susikarjan kartanolle
vielä neuvoi karhujansa susillensa suin puheli
repäise emännän reisi pure puoli pohkeata
kun tulevi katsomahan lyykistäikse lypsämähän

teki luikun lehmän luista härän sarvesta helinän
torven tuomikin jalasta pillin kirjon kinterestä
lujahutti luikullansa toitahutti torvellansa
kolmasti kotimäellä kuuesti kujosten suussa

tuop' on ilmarin emäntä sepon akka selvä nainen
viikon maiotta virovi ksävoitta kellettävi
kuuli suolta soittamisen kajahuksen kankahalta
sanovi sanalla tuolla lausui tuolla lausehella
ole kiitetty jumala torvi soipi karja saapi
mist' on orja sarven saanut torven raataja tavannut
kun tuo soitelleen tulevi toitatellen torvettavi
puhki korvani puhuvi läpi pääni läylentävi

kullervo kalervon poika sanan virkkoi noin nimesi
suolt' on orja sarven saanut tuonut torven liettehestä
jo nyt on karjasi kujalla lehmät lääväpellon päässä
saaospa savun panohon käyös lehmät lypsämähän

sepä ilmarin emäntä käski muorin lypsämähän
käypä muori lypsämähän raavahat rakentamahan
enpä itse ennättäisi taikinan alustehelta

kullervo kalervon poika sanan virkkoi noin nimesi
ainapa hyvät emännät taitavat taloiset vaimot
itse ennen lehmät lypsi itse raavahat rakenti

siitä ilmarin emäntä sai itse savupanolle
tuosta lypsylle tulevi katsoi kerran karjoansa
silmäeli siivatoita sanan virkkoi noin nimesi
karja on kaunihin näköinen siivatat sileäkarvat
kaikki ilveksen iholla metsän uuhen untuvalla
tuntuvilla tummelilla nännillä näpähyvillä

lyhmistihe lypsämähän heittihe heruttamahan
veti kerran tuosta toisen kohta kolmatta yritti
susi päälle suimastaikse karhu päälle kuopaiseikse
susi suun revittelevi karhu kiskoi kinttusuonet
puri puolen pohkeata katkoi kannan sääriluusta

kullervo kalervon poika sillä kosti piian pilkan
piian pilkan naisen naurun pahan vaimon palkan maksoi

ilmarin iso emäntä itse loihe itkemähän
sanan virkkoi noin nimesi pahoin teit sä paimo parka
ajoit kotiot kotihin suet suurille pihoille

kullervo kalervon poika tuopa tuohon vastaeli
pahoin tein mä paimen parka et hyvin emäntä parka
leivoit sie kivisen leivän kakun paistoit kallioisen
ve'in veitseni kivehen karahutin kalliohon
ainoan isoni veitsen sukukuntani kuraksen

sanoi ilmarin emäntä oi sie paimo armas paimo
myöstytäpä miettehesi perin lausu lausehesi
päästä suen suutehista karhun kynnestä kavista
mie sun paioilla parannan kaatioilla kaunistelen
syötän voilla vehnäsillärieskamaitosilla
vuoen syötän raatamatta toisen työlle työntämättä

kun et jou'u päästämähän käy pian kerittämähän
kohta kaaun kuolijaksi muutun mullan muotoiseksi

kullervo kalervon poika sanan virkkoi noin nimesi
kun on kuollet kuolkosipa kaotkosi kun kaonnet
sija on maassa mennehillä kalmassa kaonneihilla
maata mahtavaisimmanki leve'immänki levätä
