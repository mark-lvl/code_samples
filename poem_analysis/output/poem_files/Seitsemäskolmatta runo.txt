jo nyt kaukoni kuletin saatoin ahti saarelaisen
monen surman suun ohitse kalman kielen kantimetse
noille pohjolan pihoille salakansan kartanoille
nyt onpi saneltavana kielin kertoeltavana
miten lieto lemminkäinen tuo on kaunis kaukomieli
tuli pohjolan tupihin sariolan salvoksihin
ilman kutsutta pitoihin airuhitta juominkihin

tuop' on lieto lemminkäinen poika veitikkä verevä
heti kun tuli tupahan astui keskilattialle
silta liekkui lehmuksinen tupa kuusinen kumahti

sanoi lieto lemminkäinen itse virkki noin nimesi
terve tänne tultuani terve tervehyttäjälle
kuules pohjolan isäntä oisiko talossa tässä
ohria orosen purra olutta urohon juoa

itse pohjolan isäntä istui pitkän pöyän päässä
tuop' on tuolta vastoavi sanan virkkoi noin nimesi
ollevi talossa tässä tannerta orosen olla
eikä kielletä sinua jos olet siivolla tuvassa
oven suussa seisomasta oven suussa orren alla
kahen kattilan välissä kolmen koukun koskevilla

siinä lieto lemminkäinen murti mustoa haventa
kattilaisen-karvallista sanan virkkoi noin nimesi
lempo tänne lähteköhön oven suuhun seisomahan
nokianne nuohomahan karstoja karistamahan
eip' ennen minun isoni eikä valtavanhempani
seisonut sijalla sillä oven suussa orren alla
olipa sijoa silloin tanhua orihin olla
tupa pesty miesten tulla sopet luoa sormikasta
vaarnat miesten vanttuhia seinät miekkoja laella
miksip' ei ole minulle kuin ennen minun isolle

siitä siirtihen ylemmä pyörähtihe pöyän päähän
istuihe rahin nenähän petäjäisen penkin päähän
rahi vastahan rasahti petäjäinen penkki notkui

sanoi lieto lemminkäinen enpäs liene lempivieras
kun ei tuotane olutta tulevalle vierahalle

ilpotar hyvä emäntä sanan virkkoi noin nimesi
ohoh poika lemminkäisen mi sinusta vierahasta
tulit pääni polkemahan aivoni alentamahan
ohrina oluet meillä makujuomat maltahina
leipomatta vehnäleivät lihakeitot keittämättä
oisit yötä ennen tullut taikka päiveä jälestä

siinä lieto lemminkäinen murti suuta väänti päätä
murti mustoa haventa itse tuon sanoiksi virkki
jop' on täällä syömät syöty häät juotu piot pi'etty
oluet osin jaeltu me'et miehin mittaeltu
kannut kannettu kokohon tuopit roukkoihin rovittu

oi sie pohjolan emäntä pimentolan pitkähammas
pi'it häät häjyn tavalla kutsut koiran kunnialla
leipoelit leivät suuret panit ohraiset oluet
laitoit kutsut kuusianne anojat yheksiänne
kutsuit kurjat kutsuit köyhät kutsuit ruojat kutsuit
roistot
kaikki hoikat huonemiehet kaitakauhtanat kasakat
muun on kutsuit kaiken kansan  minun heitit
kutsumatta

mintähen tämä minulle omistani ohristani
muut ne kantoi kauhasilla muut ne tiiskinä tiputti
minä määrin mätkäelin puolikkoisin putkaelin
omiani ohriani kylvämiäni jyviä

en nyt liene lemminkäinen en vieras hyvän-niminen
kun ei tuotane olutta pantane pata tulelle
keittoa pa'an sisähän leiviskä sianlihoa
syöäkseni juoakseni päähän matkan päästyäni

ilpotar hyvä emäntä hänpä tuon sanoiksi virkki
ohoh piika pikkarainen orjani alinomainen
pane keittoa patahan tuo olutta vierahalle

tyttö pieni tyhjä lapsi pahin astian pesijä
lusikkojen luutustaja kapustojen kaapustaja
pani keittoa patahan luut lihoista päät kaloista
vanhat naatit naurihista kuoret leivistä kovista
toi siitä olutta tuopin kannun kaljoa pahinta
juoa lieto lemminkäisen appoa halun-alaisen
itse tuon sanoiksi virkki tokko lie sinussa miestä
juojoa tämän oluen tämän kannun kaatajata

lemminkäinen lieto poika katsoi tuosta tuoppihinsa
toukka on tuopin pohjukassa käärmehiä keskimailla
äärillä maot mateli sisiliskot liuahteli

sanoi lieto lemminkäinen kauahutti kaukomieli
tuopin tuojat tuonelahan kannun kantajat manalle
ennen kuun ylenemistä tämän päivän päätymistä

siitä tuon sanoiksi virkki oh sinä olut katala
jo nyt jou'uit joutavihin jou'uit joutavan jälille
olut suuhun juotanehe ruhkat maahan luotanehe
sormella nimettömällä vasemmalla peukalolla

tapasip' on taskuhunsa kulki kukkaroisehensa
otti ongen taskustansa väkärauan väskystänsä
tuonp' on tunki tuoppihinsa alkoi onkia olutta
maot puuttui onkehensa väkähänsä kyyt vihaiset
sa'an nosti sammakoita tuhat mustia matoja
loi ne maahan maan hyviksi kaikki laski lattialle
veti veitsensä terävän tuon on tuiman tuppirauan
sillä silpoi päät maoilta katkoi kaulat käärmehiltä
 joi oluen onneksensa me'en mustan mieliksensä
sanan virkkoi noin nimesi en mä liene lempivieras
kun ei tuotane olutta parempata juotavata
varavammalla käellä suuremmalla astialla
tahi ei oinasta isetä suurta sonnia tapeta
härkeä tupahan tuoa sorkkasäärtä huonehesen

itse pohjolan isäntä sanan virkkoi noin nimesi
mitä sie tulitki tänne ken sinua koolle kutsui

virkkoi lieto lemminkäinen sanoi kaunis kaukomieli
korea kutsuttu vieras koreampi kutsumatoin
kuules poika pohjolaisen itse pohjolan isäntä
anna ostoa olutta juomoa rahan-alaista

tuopa pohjolan isäntä tuosta suuttui ja syäntyi
kovin suuttui ja vihastui lauloi lammin lattialle
lemminkäisellen etehen sanan virkkoi noin nimesi
tuoss' on joki juoaksesi lampi laikutellaksesi

mitä huoli lemminkäinen sanan virkkoi noin nimesi
en ole vaimojen vasikka enkä härkä hännällinen
juomahan jokivesiä lampivettä lakkimahan

itse loihe loitsimahan laikahtihe laulamahan
lauloi sonnin lattialle härän suuren kultasarven
sepä lammin laikkaeli joi jokosen onneksensa

pohjolainen pitkä poika suen suustansa sukesi
senpä lauloi lattialle surmaksi lihavan sonnin

lemminkäinen lieto poika lauloi valkean jäniksen
lattialle hyppimähän sen sutosen suun e'essä

pohjolainen pitkä poika lauloi koiran koukkuleuan
tuon jäniksen tappamahan kierosilmän kiskomahan

lemminkäinen lieto poika lauloi orrelle oravan
orsilla kapahumahan koiran tuota haukkumahan

pohjolainen pitkä poika lauloi nää'än kultarinnan
näätä näppäsi oravan orren päässä istumasta

lemminkäinen lieto poika lauloi ruskean reposen
se söi nää'än kultarinnan karvan kaunihin kaotti

pohjolainen pitkä poika kanan suustansa sukesi
sillalla sipoamahan tuon reposen suun e'essä

lemminkäinen lieto poika haukan suustansa sukesi
kieleltä käpeäkynnen sepä kiskalti kanasen

sanoi pohjolan isäntä itse lausui noin nimesi
ei tässä piot paranne kun ei vierahat vähenne
talo työlle vieras tielle hyvistäki juomingista
lähe tästä hiien heitto luota kaiken ihmiskansan
kotihisi konna koita paha maahasi pakene

virkkoi lieto lemminkäinen sanoi kaunis kaukomieli
ei miestä manaten saa'a ei miestä pahempatana
sijaltansa siirtymähän paikalta pakenemahan

silloin pohjolan isäntä miekan seinältä sivalti
tempasi tuliteränsä sanan virkkoi noin nimesi
oi sie ahti saarelainen tahi kaunis kaukomieli
mitelkämme miekkojamme katselkamme kalpojamme
minunko parempi miekka vainko ahti saarelaisen

sanoi lieto lemminkäinen mitä minun on miekastani
kun on luissa lohkiellut pääkasuissa katkiellut
vaan kuitenki kaikitenki kun ei nuo piot paranne
mitelkämme katselkamme kumman miekka
mieluhumpi
eip' ennen minun isoni miekkamittoja varannut
pojastako polvi muuttui lapsesta laji väheni

otti miekan riisui rauan tempasi tuliteräisen
huotrasta huveksisesta vyöstä vennon-selkäisestä
mittelivät katselivat noien miekkojen pituutta
olipa pikkuista pitempi miekka pohjolan isännän
yhtä kynnen mustukaista puolta sormuen niveltä

sanoi ahti saarelainen virkkoi kaunis kaukomieli
sinunpa pitempi miekka sinun eellä iskeminen

siitä pohjolan isäntä sivalteli sieppaeli
tavoitteli ei tavannut lemminkäistä päälakehen
kerran ortehen osasi kamanahan kapsahutti
orsi poikki otskahutti kamana kaheksi lenti

sanoi ahti saarelainen virkkoi kaunis kaukomieli
min teki pahoa orret kamana tihua työtä
jotta orsia osoitat kamanata kapsuttelet

kuule poika pohjolaisen itse pohjolan isäntä
tukela tora tuvassa seikat akkojen seassa
tuvan uuen turmelemme lattiat verin panemme
käykämme ulos pihalle ulos pellolla torahan
tanterelle tappelohon pihalla veret paremmat
kaunihimmat kartanolla luontevaisemmat lumella

mentihin ulos pihalle tavattihin lehmän talja
levitettihin pihalle semp' on päällä seistäksensä

sanoi ahti saarelainen kuulesta sa pohjan poika
sinunpa pitempi miekka sinun kalpa kauheampi
 ehkä tuon on tarvinnetki ennenkuin ero tulevi
tahi kaula katkeavi  iske päältä pohjan poika

iski päältä pohjan poika iski kerran iski toisen
kohta kolmasti rapasi eipä oike'in osoita
lipaise lihoakana ota ei orvaskettuana

sanoi ahti saarelainen virkkoi kaunis kaukomieli
annapas minäki koitan jo se on vuoroni minunki

tuopa pohjolan isäntä ei tuosta totella ollut
yhä iski ei epäillyt tarkoitteli ei tavannut

tulta tuiski tuima rauta terä varsin valkeata
käessä lieto lemminkäisen läksi loiste loitommaksi
vasten kauloa valahti tuon on pojan pohjolaisen

sanoi kaunis kaukomieli ohoh pohjolan isäntä
niinp' on kaulasi katalan kuni koite ruskeana

tuopa poika pohjolaisen itse pohjolan isäntä
sinne siirti silmiänsä pä'in kauloa omoa
tuop' on lieto lemminkäinen siinä lyöä silpahutti
iski miestä miekallansa kavahutti kalvallansa

löip' on kerran luimahutti laski pään on päältä olka
kallon kaulalta sivalti vei kuin naatin naurihista
tahikka tähkän olesta evän kaikesta kalasta
päähyt pyörähti pihalle miehen kallo kartanolle
kuni nuolen noutaessa puusta koppelo putosi

sata oli seivästä mäellä tuhat pystössä pihalla
saoin päitä seipähissä yks' on seiväs ilman päättä
tuop' on lieto lemminkäinen otti pään pojan pätöisen
kantoi kallon kartanolta senki seipähän nenähän

siitä ahti saarelainen tuo on kaunis kaukomieli
tupahan palattuansa sanan virkkoi noin nimesi
tuo vettä vihainen piika käsiäni pestäkseni
veristä pahan isännän häjyn miehen hurmehista

pohjan akka syännyksenti syännyksenti suutuksenti
lauloi miestä miekallista asehellista urosta
saa miestä miekallista tuhat kalvan kantajata
pään varalle lemminkäisen kaukomielen kaulan päälle

jo aika tosin tulevi päivä liitolle lipuvi
toki käypi tuskemmaksi läylemmäksi lankeavi
asuskella ahti poian lemminkäisen leyhytellä
noissa pohjolan pioissa salajoukon juomingissa
