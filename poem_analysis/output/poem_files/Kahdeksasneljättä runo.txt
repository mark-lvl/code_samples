tuop' on seppo ilmarinen takoja iän-ikuinen
heitti kultaisen kuvansa hope'isen neitosensa
pisti varsan valjahisin ruskean re'en etehen
itse istuvi rekehen kohennaikse korjahansa
lähteäksensä lupasi sekä mietti mennäksensä
pyytämähän pohjolasta toista pohjolan tytärtä

sai päivän ajaneheksi tuosta toisen vierneheksi
päivälläpä kolmannella tuli pohjolan pihalle

louhi pohjolan emäntä itse päätyvi pihalle
sai tuossa sanelemahan kääntihe kyselemähän
oman lapsensa oloa asuntoa armahansa
miniänä miehelässä naisena anoppelassa

se on seppo ilmarinen alla päin pahoilla mielin
kaiken kallella kypärin sanan virkkoi noin nimesi
ellös nyt anoppiseni ellös sie kyselkö tuota
elämiä tyttäresi asuntoa armahasi
jo sen on surma suin pi'ellyt kova loppu loukahtanut
maassa on jo marjaseni kankahassa kaunoiseni
mustakulmani kulossa hopeani heinikossa
läksin toista tyttöäsi nuorempata neitoasi
annapa anoppiseni työnnä toinen tyttäresi
naisen entisen eloille sijalle sisaruensa

louhi pohjolan emäntä sanan virkkoi noin nimesi
pahoin tein minä poloinen pahoinpa polon-alainen
kun ma lapseni lupasin työnsin sulle toisenkana
nuorena nukahtamahan verevänä vieremähän
annoin kuin sutosen suuhun karhun kiljuvan kitahan

en nyt toista annakana en mä työnnä tyttöäni
nokiesi nuohojaksi karstojesi kaapijaksi
ennen työnnän tyttäreni laitan lapseni vakavan
koskehen kohisevahan palavahan pyörtehesen
manalan matikan suuhun tuonen hauin hampahisin

siitä seppo ilmarinen murti suuta väänti päätä
murti mustoa haventa käänti päätä käiväräistä
itse tunkihe tupahan alle kattojen ajoihe
sanan virkkoi noin nimesi tulepa minulle tyttö
sijalle sisaruesi naisen entisen eloille
mesileivän leipojaksi oluen osoajaksi

lauloi lapsi lattialta sekä lauloi jotta lausui
pois on liika linnastamme mies outo ovilta näiltä
tukon linnoa tuhosit palan linnoa pahensit
kerran ennen käytyäsi ovillen osattuasi

neitonen sinä sisari elä sulho'on ihastu
elä sulhon suun pitohon eläkä jalkoihin jaloihin
sulholl' on suen ikenet revon koukut kormanossa
karhun kynnet kainalossa veren juojan veitsi vyöllä
jolla päätä piirtelevi selkeä sirettelevi

neiti itse noin saneli ilmariselle sepolle
en lähe minä sinulle enkä huoli huitukoille
tapoit naisen ennen naiun surmasit sisarueni
vielä tappaisit minunki surmoaisit itseniki
onpa tässä neitosessa paremmanki miehen verta
kaunihimman varren kauppa koreamman korjan täysi
paikoille paremmillenki isommille istuimille
ei sepon sysisijoille miehen tuhmaisen tulille

se on seppo ilmarinen takoja iän-ikuinen
murti suuta väänti päätä murti mustoa haventa
saautti tytön samassa käärälti käpälihinsä
läksi tuiskuna tuvasta riepsahti rekensä luoksi
työnnälti tytön rekehen koksahutti korjahansa
läksi kohta kulkemahan valmistui vaeltamahan
käsi ohjassa orosen toinen neien nännisillä

neiti itki ja urisi sanan virkkoi noin nimesi
sain nyt suolle karpalohon vehkahan vesiperille
tuonne ma kana katoan kuolen lintu liian surman

kuule seppo ilmarinen kun et laskene minua
potkin korjasi paloiksi sären reen repalehiksi
potkin poikki polvillani sären säärivarsillani

se on seppo ilmarinen itse tuon sanoiksi virkki
sentähen sepon rekosen laiat rautahan rakettu
jotta potkia pitävi hyvän immen heiskaroia

neitonen kujertelevi vyö vaski valittelevi
sormiansa murtelevi katkovi kätösiänsä
sanan virkkoi noin nimesi kun et laskene minua
laulaime meren kalaksi syvän aallon siikaseksi

se on seppo ilmarinen itse tuon sanoiksi virkki
etpä sinä sinne pääse minä haukina jälessä

neitonen kujertelevi vyö vaski valittelevi
sormiansa murtelevi katkovi kätösiänsä
sanan virkkoi noin nimesi kun et laskene minua
metsähän menetteleime kärpäksi kiven kolohon

se on seppo ilmarinen itse tuon sanoiksi virkki
etpä sinä sinne pääse minä saukkona jälessä

neitonen kujertelevi vyö vaski valittelevi
sormiansa murtelevi katkovi kätösiänsä
sanan virkkoi noin nimesi kun et laskene minua
kiuruna kiverteleime taaksi pilven piilemähän

se on seppo ilmarinen itse tuon sanoiksi virkki
etpä sinä sinne pääse minä kokkona jälessä

kulki matkoa palasen ajoi tietä pikkuruisen
jo hepo höryeleikse luppakorva luonteleikse

neiti päätänsä kohotti näki jälkiä lumessa
kysytteli lausutteli mi on tästä poikki juosnut
sanoi seppo ilmarinen jänö on juosnut siitä poikki

neiti parka huokaiseikse huokaiseikse henkäiseikse
sanan virkkoi noin nimesi voi minua kurja raukka

parempi minun olisi parempi oletteleisi
jänön juoksevan jälillä koukkupolven polkemilla
kuin tämän kosijan reessä viirunaaman viltin alla
jänön on karvat kaunihimmat jänön suumalo somempi

se on seppo ilmarinen puri huulta väänti päätä
ajoa kahattelevi ajoi matkoa palasen
taas hepo höryeleikse luppakorva luonteleikse

neiti päätänsä kohotti näki jälkiä lumessa
kysytteli lausutteli mi on tästä poikki juosnut
sanoi seppo ilmarinen repo on juosnut siitä poikki

neiti parka huokaiseikse huokaiseikse henkäiseikse
sanan virkkoi noin nimesi voi minua kurja raukka
parempi minun olisi parempi oletteleisi
revon reyhkävän re'essä aina käyvän ahkiossa
kuin tämän kosijan reessä viirunaaman viltin alla
revon on karvat kaunihimmat revon suumalo
somempi

se on seppo ilmarinen puri huulta väänti päätä
ajoa kahattelevi ajoi matkoa palasen
taas hepo höryeleikse luppakorva luonteleikse

neiti päätänsä kohotti näki jälkiä lumessa
kysytteli lausutteli mi on tästä poikki juosnut
sanoi seppo ilmarinen hukka on juosnut siitä poikki

neiti parka huokaiseikse huokaiseikse henkäiseikse
sanan virkkoi noin nimesi voi minua kurja raukka
parempi minun olisi parempi oletteleisi
hukan hurskavan jälillä alakärsän askelilla
kuin tämän kosijan reessä viirunaaman viltin alla
hukan on karva kaunihimpi hukan suumalo somempi

se on seppo ilmarinen puri huulta väänti päätä
ajoa kahattelevi yöksi uutehen kylähän

matkalta väsynehenä seppo nukkuvi sike'in
toinen naista naurattavi mieheltä unekkahalta

siitä seppo ilmarinen aamulla havattuansa
murti suuta väänti päätä murti mustoa haventa
sanoi seppo ilmarinen itse mietti noin nimesi
joko luome laulamahan laulan moisen morsiamen
metsähän metsän omaksi vai vetehen veen omaksi

en laula metsän omaksi metsä kaikki kaihostuisi
enkäpä ve'en omaksi vieroaisi veen kalaset
ennen kaa'an kalvallani menettelen miekallani

miekka mietti miehen kielen arvasi uron pakinan
sanan virkkoi noin nimesi ei liene minua luotu
naisia menettämähän kataloita kaatamahan

se on seppo ilmarinen jopa loihe laulamahan
syäntyi sanelemahan lauloi naisensa lokiksi
luo'olle lokottamahan veen karille kaikkumahan
nenät nienten niukumahan vastatuulet vaapumahan

siitä seppo ilmarinen rekehensä reutoaikse
ajoa kahattelevi alla päin pahoilla mielin
matkasi omille maille tuli maille tuttaville

vaka vanha väinämöinen tiellä vastahan tulevi
sai tuosta sanelemahan veli seppo ilmarinen
mit' olet pahoilla mielin kahta kallella kypärin
pohjolasta tullessasi miten pohjola elävi

sanoi seppo ilmarinen mi on pohjolan eleä
siell' on sampo jauhamassa kirjokansi kallumassa
päivän jauhoi syötäviä päivän toisen myötäviä
kolmannen kotipitoja

jotta sanon kuin sanonki vielä kerran kertaelen
mi on pohjolan eleä kun on sampo pohjolassa
siin' on kyntö siinä kylvö siinä kasvo kaikenlainen
siinäpä ikuinen onni

sanoi vanha väinämöinen veli seppo ilmarinen
minne heitit naisen nuoren kunne kuulun morsiamen
kun sa tyhjänä tuletki aina naisetta ajelet

se on seppo ilmarinen sanan virkkoi noin nimesi
lauloin ma mokoman naisen meren luo'olle lokiksi
nyt se lokkina lojuvi kajavana kaakahtavi
y kiljuvi vesikivillä kariloilla kaljahuvi
