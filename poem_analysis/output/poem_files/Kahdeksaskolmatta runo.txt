jo nyt ahti saarelainen itse lieto lemminkäinen
pistäiksen on piilemähän painaikse pakenemahan
pimeästä pohjolasta sangasta saran talosta

läksi tuiskuna tuvasta savuna pihalle saapi
pakohon pahoja töitä pillojansa piilemähän

niin pihalle tultuansa katseleikse käänteleikse
etsi entistä oritta näe ei entistä oroa
näki paaen pellon päässä pajupehkon pientarella

mikäs neuvoksi tulevi mikä neuvon antajaksi
ettei pää pahoin menisi tukka turhi'in tulisi
hivus hieno lankeaisi näillä pohjolan pihoilla
jo kumu kylästä kuului tomu toisista taloista
välkytys kylän väliltä silmän isku ikkunoilta

tuossa lieto lemminkäisen tuon on ahti saarelaisen
täytyi toisiksi ruveta piti muuksi muutellaita
kokkona ylös kohosi tahtoi nousta taivahalle
päivä poltti poskipäitä kuuhut kulmia valaisi

siinä lieto lemminkäinen ukkoa rukoelevi
oi ukko hyvä jumala mies on tarkka taivahinen
jymypilvien pitäjä hattarojen hallitsija
laaipa utuinen ilma luopa pilvi pikkarainen
jonka suojassa menisin kotihini koitteleisin
luoksi ehtoisen emoni tykö valtavanhempani

lenteä lekuttelevi katsoi kerran jälkehensä
keksi harmoan havukan  sen silmät paloi tulena
kuni pojan pohjolaisen pohjan entisen isännän

sanoi harmoa havukka ohoh ahti veikkoseni
muistatko muinaista sotoa tasapäätä tappeloa

sanoi ahti saarelainen virkkoi kaunis kaukomieli
havukkani lintuseni käännäite kohin kotia
sano tuonne tultuasi pimeähän pohjolahan
'kova on kokko kourin saa'a kynälintu kynsin syöä'

jo kohta kotihin joutui luoksi ehtoisen emonsa
suulla surkeannäöllä syämellä synkeällä

emo vastahan tulevi kulkiessansa kujoa
aitoviertä astuessa ennätti emo kysyä
poikueni nuorempani lapseni vakavampani
mit' olet pahoilla mielin pohjolasta tullessasi
onko sarkoin vaarrettuna noissa pohjolan pioissa
jos on sarkoin vaarrettuna saat sinä paremman sarkan
taattosi soasta saaman tavoittaman tappelosta

sanoi lieto lemminkäinen oi emoni kantajani
ken mun sarkoin vaarteleisi itse vaartaisin isännät
vaartaisin sata urosta tuhat miestä tunnustaisin

sanoi äiti lemminkäisen mit' olet pahoilla mielin
oletko voitettu orihin herjattu hevosen varsoin
jos olet voitettu orihin ostaos ori parempi
ison saamilla eloilla vanhemman varustamilla

sanoi lieto lemminkäinen oi emoni kantajani
ken mun herjaisi hevosin eli varsoin voitteleisi
itse herjaisin isännät voittaisin oron ajajat
miehet vankat varsoinensa urohot orihinensa

sanoi äiti lemminkäisen mit' olet pahoilla mielin
kuta synke'in syämin pohjolasta tultuasi
oletko naisin naurettuna eli piioin pilkattuna
jos olet naisin naurettuna eli piioin pilkattuna
toiset toiste pilkatahan naiset vasta nauretahan

sanoi lieto lemminkäinen oi emoni kantajani
ken mun naisin naurattaisi eli piioin pilkkoaisi
itse nauraisin isännät kaikki piiat pilkkoaisin
nauraisin sataki naista tuhat muuta morsianta

sanoi äiti lemminkäisen mi sinulla poikueni
on sulle satunen saanut pohjolassa käyessäsi
vainko liioin syötyäsi syötyäsi juotuasi
olet öisillä sijoilla nähnyt outoja unia

silloin lieto lemminkäinen sai tuossa sanoneheksi
akat noita arvelkohot öisiä unennäköjä
muistan yölliset uneni sen paremmin päivälliset
oi emoni vanha vaimo sääli säkkihin evästä
pane jauhot palttinahan suolat riepuhun sovita
pois tuli pojalle lähtö matka maasta ottaminen
tästä kullasta ko'ista kaunihista kartanosta
miehet miekkoja hiovat kärestävät keihä'itä

emo ennätti kysyä vaivan nähnyt vaaitella
miksi miekkoja hiovat kärestävät keihä'itä

virkkoi lieto lemminkäinen sanoi kaunis kaukomieli
siksi miekkoja hiovat kärestävät keihä'itä
mun poloisen pään varalle vasten kauloa katalan
tuli työ tapahtui seikka noilla pohjolan pihoilla
tapoin pojan pohjolaisen itsen pohjolan isännän
nousi pohjola sotahan takaturma tappelohon
vasten vaivaista minua yksinäisen ympärille

emo tuon sanoiksi virkki lausui vanhin lapsellensa
jo sanoin minä sinulle jo vainen varoittelinki
yhä kielteä käkesin lähtemästä pohjolahan
mahoit olla oikeassa eleä emon tuvilla
oman vanhemman varassa kantajasi kartanossa
ei oisi sotoa saanut tapahtunut tappeloa

kunne nyt poikani poloinen kunne kannettu katala
lähet pillan piilentähän työn pahan pakenentahan
ettei pää pahoin menisi kaula kaunis katkeaisi
tukka turhi'in tulisi hivus hieno lankeaisi

sanoi lieto lemminkäinen en tieä sitä sijoa
kunne painuisin pakohon pillojani piilemähän
oi emoni kantajani kunne käsket piilemähän

sanoi äiti lemminkäisen itse lausui noin nimesi
en mä tieä kunne käsken kunne käsken ja kehoitan
menet männyksi mäelle katajaksi kankahalle
tuho sielläki tulevi kova onni kohtoavi
use'in mäkinen mänty pärepuiksi leikatahan
usei'in kataja kangas seipähiksi karsitahan

nouset koivuksi norolle tahikka lehtohon lepäksi
tuho sielläki tulisi kova onni kohti saisi
use'in noroinen koivu pinopuiksi pilkotahan
use'in lepikkölehto hakatahan halmeheksi

menet marjaksi mäelle puolukaksi kankahalle
näille maille mansikoiksi mustikoiksi muille maille
tuho sielläki tulisi kova onni kohtoaisi
noppisivat nuoret neiet tinarinnat riipisivät

mene hauiksi merehen siiaksi silajokehen
tuho sielläki tulisi kova loppu loukahtaisi
mies nuori noentolainen veisi verkkonsa vesille
nuoret nuotalla vetäisi vanhat saisi verkollansa

menet metsähän sueksi korpimaille kontioksi
tuho sielläki tulisi kova onni kohtoaisi
mies nuori noen näköinen kärestäisi keihä'änsä
surmataksensa sutoset metsän karhut kaataksensa

silloin lieto lemminkäinen sanan virkkoi noin nimesi
itse tieän ilke'immät paikat arvoan pahimmat
kussa surma suin pitäisi kova loppu loukahtaisi
oi emo elättäjäni maammo maion-antajani
kunne käsket piilemähän kunne käsket ja kehoitat
aivan on surma suun e'essä paha päivä parran päällä
yksi päivä miehen päätä tuskin täytehen sitänä

silloin äiti lemminkäisen itse virkki noin nimesi
sanon ma hyvänki paikan ani armahan nimitän
missä piillä pillomuksen paeta pahan-alaisen
muistan maata pikkuruisen tieän paikkoa palasen
syömätöintä lyömätöintä miekan miehen käymätöintä
sie vanno valat ikuiset valehettomat vakaiset
kuunna kymmennä kesänä et sotia käyäksesi
hopeankana halulla tahi kullan tarpehella

sanoi lieto lemminkäinen vannon mie valat vakaiset
en kesänä ensimäisnä tok' en vielä toisnakana
saa'a suurihin sotihin noihin miekan melskehisin
viel' on haavat hartioissa syvät reiät ryntähissä
entisistäkin iloista mennehistä melskehistä
suurilla sotamä'illä miesten tappotanterilla

silloin äiti lemminkäisen sanan virkkoi noin nimesi
otapa isosi pursi lähe tuonne piilemähän
ylitse meren yheksän meri-puolen kymmenettä
saarehen selällisehen luotohon merellisehen
siell' ennen isosi piili sekä piili jotta säilyi
suurina sotakesinä vainovuosina kovina
hyvä oli siellä ollaksensa armas aikaellaksensa
siellä piile vuosi toinen käy kotihin kolmannella
tutuille ison tuville vanhempasi valkamoille
