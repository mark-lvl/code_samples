sai sanoma pohjolahan tieto kylmähän kylähän
väinölän vironneheksi kalevalan pääsneheksi
noista nostamavioista tauista tavattomista

louhi pohjolan emäntä pohjan akka harvahammas
tuo tuosta kovin pahastui sanan virkkoi noin nimesi
vielä muistan muunki keinon toki toisen tien osoan
nostan karhun kankahalta korvesta koverakouran
päälle väinölän elojen kalevalan karjan päälle

nosti karhun kankahalta kontion kovilta mailta
noille väinölän ahoille kalevalan karjamaille

vaka vanha väinämöinen itse tuon sanoiksi virkki
veli seppo ilmarinen taos mulle uusi keihäs
tao keiho kolmisulka varren vaskisen keralla
ois' otso otettavana rahakarva kaattavana
ruuniani ruhtomasta tammojani tahtomasta
kaatamasta karjoani lehmiä levittämästä

seppo keihyen takovi eikä pitkän ei lyhyen
takoi keskilaaullisen sen susi sulalla seisoi
kontio terän kohalla hirvi hiihti suoverossa
varsa varrella samosi peura potki ponnen päässä

satoi siitä uutta lunta hiukan hienoista vitiä
sykysyisen uuhen verran verran talvisen jäniksen
sanoi vanha väinämöinen itse virkki noin nimesi
mieleni minun tekevi mieli käyä metsolassa
metsän tyttöjen tykönä sinipiikojen pihoilla

lähen miehistä metsälle urohista ulkotöille
ota metsä miehiksesi urohiksesi tapio
auta onni ottamahan metsän kaunis kaatamahan

mielikki metsän emäntä tellervo tapion vaimo
kytke kiinni koiroasi rakentele rakkiasi
kuusamisehen kujahan talahasen tammisehen

otsonen metsän omena mesikämmen källeröinen
kun kuulet minun tulevan miehen aimo astelevan
kytke kynnet karvoihisi hampahat ikenihisi
ettei koske konsakana liikuta lipeänänä

otsoseni ainoiseni mesikämmen kaunoiseni
lyöte maata mättähälle kaunihille kalliolle
hongat päällä huojumassa kuuset päällä kuulumassa
siinä otso pyörteleite mesikämmen käänteleite
kuni pyy pesänsä päällä hanhi hautomaisillansa

siinä vanha väinämöinen kuuli koiran haukkuvaksi
penun julki juttavaksi pikkusilmäisen pihalla
tasakärsän tanhu'illa sanan virkkoi noin nimesi
luulin kukkuvan käkösen lempilinnun laulelevan
ei käki kukahakana lempilintu laulakana
tääll' on koirani komehin otukseni oivallisin
otsosen tuvan ovella miehen kaunon kartanolla

vaka vanha väinämöinen siinä otsosen tapasi
säteriset sängyt kaati sijat kultaiset kumosi
sanovi sanalla tuolla lausui tuolla lausehella
ole kiitetty jumala ylistetty luoja yksin
kun annoit otson osaksi salon kullan saalihiksi

katselevi kultoansa sanan virkkoi noin nimesi
otsoseni ainoiseni mesikämmen kaunoiseni
elä suutu suottakana en minä sinua kaannut
itse vierit vempeleltä hairahit havun selältä
puhki puiset kaatiosi halki haljakan havuisen
sykysyiset säät lipeät päivät pilviset pimeät

metsän kultainen käkönen kaunis karva röyhetyinen
heitä nyt kylmille kotosi asuinmaasi autiaksi
koivunoksainen kotosi vasunvarpainen majasi
lähe kuulu kulkemahan metsän auvo astumahan
käymähän käpeäkenkä sinisukka sipsomahan
näiltä pieniltä pihoilta kape'ilta käytäviltä
urohoisehen väkehen miehisehen joukkiohon
ei siellä pahoin pi'etä ei eletä kehnon lailla
sima siellä syötetähän mesi nuori juotetahan
tulevalle vierahalle saavalle käkeävälle

lähe nyt tästä kuin lähetki tästä pienestä pesästä
alle kuulun kurkihirren alle kaunihin katoksen
niin sä luikkaos lumella kuni lumme lammin päällä
niin sä haihaos havulla kuni oksalla orava

siitä vanha väinämöinen laulaja iän-ikuinen
astui soitellen ahoja kajahellen kankahia
kera kuulun vierahansa kanssa karvalallusensa
jo soitto tupahan kuului alle kattojen kajahus

virkahti väki tuvassa kansa kaunis vieretteli
kuulkottes tätä kumua salon soittajan sanoja
käpylinnun kälkytystä metsän piian pillin ääntä

vaka vanha väinämöinen itse ennätti pihalle
vierähti väki tuvasta kansa kaunis lausutteli
joko on kulta kulkemassa hopea vaeltamassa
rahan armas astumassa tenka tietä poimimassa
mesijänkö metsä antoi ilveksen salon isäntä
koska laulaen tulette hyreksien hiihtelette

vaka vanha väinämöinen tuossa tuon sanoiksi virkki
sanomiks' on saukko saatu virsiksi jumalan vilja
sillä laulaen tulemme hyreksien hiihtelemme

eikä saukko ollekana eikä saukko eikä ilves
itse on kuulu kulkemassa salon auvo astumassa
mies vanha vaeltamassa verkanuttu vieremässä
kun lie suotu vierahamme ovet auki paiskatkatte
vaan kun lie vihattu vieras kiinni lyökätte lujahan

väki vastaten sanovi kansa kaunis vieretteli
terve otso tultuasi mesikämmen käytyäsi
näille pestyille pihoille kaunoisille kartanoille

tuota toivoin tuon ikäni katsoin kaiken kasvinaian
soivaksi tapion torven metsän pillin piukovaksi
kulkevaksi metsän kullan saavaksi salon hopean
näille pienille pihoille kape'ille käytäville

toivoin kuin hyveä vuotta katsoin kuin kesän tuloa
niinkuin suksi uutta lunta lyly liukasta lipua
neiti nuorta sulhokaista punaposki puolisoa

illat istuin ikkunoissa aamut aitan portahilla
veräjillä viikkokauet kuukauet kujaisten suussa
talvikauet tanhu'illa lumet seisoin tanteriksi
tanteret suliksi maiksi sulat maat somerikoiksi
somerikot hiesukoiksi hiesukot vihottaviksi
ajattelin aamut kaiket päivät päässäni pitelin
missä viikon otso viipyi salon armas aikaeli
oisiko virohon viernyt maasta suomen sorkehtinut

siitä vanha väinämöinen itse tuon sanoiksi virkki
minne vienen vierahani kulettanen kultaiseni
tokko laittanen latohon pannen pahnahuonehesen

väki vastaten sanovi kansa kaunis vieretteli
tuonne vienet vierahamme kulettanet kultaisemme
alle kuulun kurkihirren alle kaunihin katoksen
siell' on syömät suoriteltu juomaneuvot jou'uteltu
kaikki sillat siivottuna lakaistuna lattiaiset
kaikki vaimot vaatehtinna pukemihin puhtahisin
sore'ihin pääsomihin valke'ihin vaattehisin

siitä vanha väinämöinen itse virkki noin nimesi
otsoseni lintuseni mesikämmen kääröseni
viel' on maata käyäksesi kangasta kavutaksesi

lähes nyt kulta kulkemahan armas maata astumahan
mustasukka muikumahan verkahousu vieremähän
käymähän tiaisen teitä varpusen vaeltamia
alle viien viilohirren alle kuuen kurkiaisen

varo'otte vaimo raukat ettei karja kammastuisi
pieni vilja pillastuisi vikoisi emännän vilja
tullessa otson tuville karvaturvan tunkeitessa

pois on pojat porstuasta piiat pihtipuolisista
uron tullessa tupahan astuessa aimo miehen

metsän otsonen omena metsän kaunis källeröinen
ellös piikoja pelätkö kassapäitä kammastelko
eläkä vaimoja varoa sure sylttysukkaisia
mi on akkoja tuvassa ne on kaikki karsinahan
miehen tullessa tupahan astuessa aika poian

sanoi vanha väinämöinen terve tänneki jumala
alle kuulun kurkiaisen alle kaunihin katoksen
mihin nyt heitän hempuseni lasken karvalalluseni

väki vastahan sanovi terve terve tultuasi
tuohon liitä lintusesi kulettele kultaisesi
petäjäisen pienan päähän rautaisen rahin nenähän
turkin tunnusteltavaksi karvojen katseltavaksi

elä otso tuosta huoli eläkä pane pahaksi
jos tulevi turkin tunti karvojen katsanto-aika
ei tuhota turkkiasi karvojasi ei katsota
herjojen hetalehiksi vaivaisien vaattehiksi

siitä vanha väinämöinen otatti otsolta turkin
pani aitan parven päähän lihat liitti kattilahan
kuparihin kullattuhun vaskipohjahan patahan

jo oli pa'at tulella vaskilaiat valkealla
täpittynä täytettynä liioilla lihamuruilla
suolat saatettu sekahan jotk' oli tuotu tuonnempata
saatu suolat saksanmaalta vienan pääliltä vesiltä
souttu suolasalmen kautta laivan päältä laskettuna

kun oli keitto keitettynä saatu kattilat tulelta
jopa saalis saatettihin käpylintu käytettihin
päähän pitkän pintapöyän kultaisihin kuppiloihin
simoa sirettämähän olosia ottamahan

petäjäst' oli pöytä tehty va'it vaskesta valettu
lusikkaiset hopeasta veitset kullasta kuvattu
kupit kaikki kukkusilla va'it varpelaitasilla
metsän mieliantehia salon kullan saalihia

siinä vanha väinämöinen itse tuon sanoiksi virkki
kummun ukko kultarinta tapion talon isäntä
metsolan metinen vaimo metsän ehtoisa emäntä
mies puhas tapion poika mies puhas punakypärä
tellervo tapion neiti kanssa muu tapion kansa
tule nyt häihin härkösesi pitkävillasi pitoihin
nyt on kyllin kystä syöä kyllin syöä kyllin juoa
kyllin itsensä piteä kyllin antoa kylälle

väki tuossa noin sanovi kansa kaunis vieretteli
miss' on otso syntynynnä rahankarva kasvanunna
tokko tuo olilla syntyi kasvoi saunan karsinassa

silloin vanha väinämöinen itse tuon sanoiksi virkki
ei otso olilla synny eikä riihiruumenilla
tuoll' on otso synnytelty mesikämmen käännytelty
luona kuun malossa päivän otavaisen olkapäillä
ilman impien tykönä luona luonnon tyttärien

astui impi ilman äärtä neiti taivahan napoa
kävi pilven piirtä myöten taivahan rajoa myöten
sukassa sinertävässä kirjavassa kaplukassa
villavakkanen käessä karvakoppa kainalossa
viskoi villan pään vesille laski karvan lainehille
tuota tuuli tuuitteli ilma lieto liikutteli
ve'en henki heilutteli aalto rannalle ajeli
rannalle salon simaisen nenähän metisen niemen

mielikki metsän emäntä tapiolan tarkka vaimo
koppoi kuontalon vesiltä villat hienot lainehilta

siitä liitti liukkahasti kapaloitsi kaunihisti
vaahterisehen vasuhun kaunoisehen kätkyehen
nostatti kapalonuorat vitjat kultaiset kuletti
oksalle olovimmalle lehvälle leve'immälle

tuuitteli tuttuansa liekutteli lempeänsä
alla kuusen kukkalatvan alla penseän petäjän
siinä otsosen sukesi jalokarvan kasvatteli
vieressä metisen viian simaisen salon sisässä

kasvoi otso kaunihiksi yleni ylen ehoksi
lyhyt jalka lysmä polvi tasakärsä talleroinen
pää levyt nenä nykerä karva kaunis röyhetyinen
ei ollut vielä hampahia eikä kynsiä kyhätty

mielikki metsän emäntä itse tuon sanoiksi virkki
'kyheäisin kynnet tuolle kanssa hampahat hakisin
kun tuo ei vioille saisi painuisi pahoille töille'

niin otso valansa vannoi polvilla metsän emännän
eessä julkisen jumalan alla kasvon kaikkivallan
ei tehäksensä pahoa ruveta rumille töille

mielikki metsän emäntä tapiolan tarkka vaimo
läksi hammasta hakuhun kynsiä kyselemähän
pihlajilta piuke'ilta katajilta karke'ilta
jukaisilta juurikoilta kesunkannoilta kovilta
eipä sieltä kynttä saanut eikä hammasta tavannut

honka kasvoi kankahalla kuusi kummulla yleni
hongassa hopeaoksa kultaoksa kuusosessa
ne kapo käsin tavoitti niistä kynsiä kyhäsi
niitä liitti leukaluuhun ikenihin istutteli

siitä laski lallokkinsa ulos lempensä lähetti
pani suota soutamahan viitoa vitaisemahan
ahoviertä astumahan kangasta kapuamahan
käski käyä kaunihisti soreasti sorkutella
elellä ajat iloiset kulutella kuulut päivät
suon selillä maan navoilla kisakangasten perillä
käyä kengättä kesällä sykysyllä syylingittä
asua ajat pahemmat talvikylmät kyhmästellä
tuomisen tuvan sisässä havulinnan liepehellä
kengällä korean kuusen katajikon kainalossa
alla viien villavaipan alla kaapuan kaheksan

sieltä sain nyt saalihini ehätin tämän eräni

väki nuori noin sanovi väki vanha virkkelevi
mitä tehen metsä mieltyi metsä mieltyi korpi kostui
ihastui salon isäntä taipui ainoinen tapio
jotta antoi ainokkinsa menetti mesikkisensä
oliko keihon keksimistä eli nuolen noutamista

vaka vanha väinämöinen itse tuon sanoiksi virkki
hyvin meihin metsä mieltyi metsä mieltyi korpi kostui
ihastui salon isäntä taipui ainoinen tapio

mielikki metsän emäntä tellervo tapion neiti
metsän neiti muoto kaunis metsän piika pikkarainen
läksi tietä neuvomahan rastia rakentamahan
tien vieriä viittomahan matkoa opastamahan
veisti pilkat pitkin puita rastit vaaroihin rakenti
jalon otsosen oville rahasaaren rantehille

sitte sinne tultuani perillen osattuani
ei ollut keihon keksimistä ampuen ajelemista
itse vieri vempeleltä horjahti havun selältä
risut rikkoi rintapäänsä varvut vatsansa hajotti

siitä tuon sanoiksi virkki itse lausui noin nimesi
otsoseni ainoiseni lintuseni lempiseni
päästä nyt tänne pääripasi pujota puraisimesi
heitä harvat hampahasi liitä leukasi leveät
eläkä pane pahaksi jos meille mikä tulisi
luien luske päien pauke kova hammasten kolina

jo otan nenän otsolta nenän entisen avuksi
en ota osattomaksi enkä aivan ainoaksi

otan ma otsolta korvan korvan entisen avuksi
en ota osattomaksi enkä aivan ainoaksi

otan ma otsolta silmän silmän entisen avuksi
en ota osattomaksi enkä aivan ainoaksi

otan ma otsan otsolta otsan entisen avuksi
en ota osattomaksi enkä aivan ainoaksi

otan ma otsolta turvan turvan entisen avuksi
en ota osattomaksi enkä aivan ainoaksi

otan ma otsolta kielen kielen entisen avuksi
en ota osattomaksi enkä aivan ainoaksi

sen nyt mieheksi sanoisin urohoksi arvoaisin
joka umpiluut lukisi saisi sarjahampahuiset
leuasta teräksisestä rusamilla rautaisilla

eipä toista tullutkana ei ollut urosta tuota
itse umpiluut lukevi sarjahampahat sanovi
alla luisten polviensa rautaisten rusamiensa

otti hampahat otsolta sanan virkkoi noin nimesi
metsän otsonen omena metsän kaunis källeröinen
nyt on matka käyäksesi retki reiahellaksesi
tästä pienestä pesästä matalaisesta majasta
korkeampahan kotihin avarampahan asuhun

lähe nyt kulta kulkemahan rahan armas astumahan
sivutse sikojen teistä poikki porsasten poluista
vasten varvikkomäkeä kohti vuorta korkeata
petäjähän penseähän honkahan havusatahan
hyvä siin' on ollaksesi armas aikaellaksesi
kuuluvilla karjan kellon luona tiukujen tirinän

vaka vanha väinämöinen jo tuli kotihin tuolta
väki nuori noin sanovi kansa kaunis lausutteli
minne saatit saalihisi kunne ennätit eräsi
lienet jäälle jättänynnä uhkuhun upottanunna
suomutihin sortanunna kaivanunna kankahasen

vaka vanha väinämöinen sanan virkkoi noin nimesi
enpä jäälle jättänynnä uhkuhun upottanunna
siinä koirat siirteleisi linnut liiat peitteleisi
enkä suohon sortanunna kaivanunna kankahasen
siinä toukat turmeleisi söisi mustat muurahaiset

tuonne saatin saalihini ehätin erän vähäni
kultakunnahan kukulle vaskiharjun hartioille

panin puuhun puhtahasen honkahan havusatahan
oksalle olovimmalle lehvälle leve'immälle
iloksi inehmisille kunnioiksi kulkijoille

ikenin panin itähän silmin loin on luotehesen
enkä aivan latvasehen oisin luonut latvasehen
siinä tuuli turmeleisi ahava pahoin panisi
enkä pannut maavarahan oisin pannut maavarahan
siat siinä siirteleisi alakärsät käänteleisi

siitä vanha väinämöinen laikahtihe laulamahan
illan kuulun kunniaksi päivän päätyvän iloksi

sanoi vanha väinämöinen itse lausui noin nimesi
piä nyt pihti valkeata jotta lauloa näkisin
lauloa luku tulevi suuni soia tahtelevi

siinä lauloi jotta soitti pitkin iltoa iloitsi
lausui laulunsa lopulla itse virkki viimeiseksi
anna toisteki jumala vastaki vakainen luoja
näin näissä ilottavaksi toiste toimiteltavaksi
näissä häissä pyylypoian pitkävillaisen pioissa

anna ainaki jumala toisteki totinen luoja
rastia rakettaviksi puita pilkoteltaviksi
urohoisessa väessä miehisessä joukkiossa

anna ainaki jumala toisteki totinen luoja
soivaksi tapion torven metsän pillin piukovaksi
näillä pienillä pihoilla kape'illa kartanoilla

päivät soisin soitettavan illat tehtävän iloa
näillä mailla mantereilla suomen suurilla tiloilla
nuorisossa nousevassa kansassa kasuavassa
