jopa viikon vuoteltihin vuoteltihin katseltihin
neion nuotehet tulevan seppo ilmarin kotihin
silmät vanhoilta valuvi ikkunoissa istuessa
polvet nuorilta nojuvi veräjillä vuottaessa
lasten jalkoja paleli seinuksilla seisoessa
kului kengät kesk'-iältä rantasilla raittaessa

niin huomenna muutamana päivänä moniahana
kumu kuuluvi salolta reen kapina kankahalta

lokka luopuisa emäntä kalevatar kaunis vaimo
sanan virkkoi noin nimesi se on poikani rekonen
jo tulevi pohjolasta nuoren neitonsa keralla

lähes nyt kohti näitä maita kohin näitä kartanoita
ison saamille tuville vanhemman varustamille

se on seppo ilmarinen jo kohta kotihin saapi
ison saamille pihoille vanhemman varustamille
pyyhyet vihertelevät vesaisilla vempelillä
käkyet kukahtelevat korjan kirjavan kokalla
oravat samoelevat päällä aisan vaahterisen

lokka luopuisa emäntä kalevatar vaimo kaunis
tuossa tuon sanoiksi virkki itse lausui noin nimesi
kylä vuotti uutta kuuta nuoret päivän nousentoa
lapset maata mansikkaista vesi tervaista venettä
mie en kuuta puolinkana päiveä mokominkana
minä vuotin veijoani veijoani minjoani
katsoin aamun katsoin illan en tiennyt mihin katosi
tokko pientä kasvatteli vaiko laihoa lihoitti
kun ei tullut kuitenkana vaikka varsinki lupasi
tulla jälen tuntuessa saa'a jälen jäähtymättä

aina katsoin aamusilla päivät päässäni pitelin
kun ei vieri veijon saani ei kolaja veijon korja
näille pienille pihoille kape'ille kartanoille
oisko olkinen oronen reki kaksikaplahinen
senki saaniksi sanoisin korjaksi korotteleisin
jos se veijoni vetäisi toisi kaunoni kotihin

niinpä toivoin tuon ikäni katsoin kaiken päiväkauen
pääni katsoin kallellehen sykeröni syrjällehen
silmät suorat suikulaksi toivoin veijoni tulevan
näille pienille pihoille kape'ille kartanoille
jo tuo viimeinki tulevi toki kerran kerkiävi
vierellä verevä muoto punaposki puolellansa

sulho viljon veljyeni lasketapa laukkiotsa
vietätä hyvä hevonen entisille heinillensä
taanoisille kauroillensa laai meille terveyttä
laai meille laai muille laai kaikelle kylälle

tehtyäsi tervehyiset sanele tarinojasi
matkasitko mainehitta kävit tiesi tervehenä
kun läksit anopin luoksi apen ainoan kotihin
saitko neien voitit vallan sorritko sotiveräjän
levititkö neien linnan pirotitko pystyn seinän
astuitko anopin sillan istuitko isännän lautsan

jo tuon näen kyselemättä arvoan anelematta
kävipä tiensä tervehenä matkansa imantehena
toip' on hanhen voitti vallan sortipa sotiveräjän
langettipa lautalinnan levitteli lehmusseinän
käyessä anopin luona apen ainoan ko'issa
onp' on sotka suojassansa kana kainaloisessansa
puhas neiti puolellansa valkeainen valloissansa

kenpä toi tämän valehen ken pani pahan sanoman
sulhon tyhjin tulleheksi oron jouten juosneheksi
eipä sulho tyhjin tullut ei oronen jouten juosnut
on mitä oron veteä liinaharjan liikutella
hiessäpä hyvä hevonen vaahessa valittu varsa
tuvun tänne tuotuansa verevän ve'ettyänsä

nouse nyt korjasta korea hyvä lahja laitiosta
nouse ilman nostamatta ylene ylentämättä
jos on nuori nostajasi ylpeä ylentäjäsi

korjasta kohottuasi reen perästä päästyäsi
astu tietä temminkäistä maata maksankarvallista
sikojen silittämäistä porsahien polkemaista
lampahan latsottamaista hevon harjan hieromaista

astu hanhen askelilla taputa tavin jaloilla
näitä pestyjä pihoja tasaisia tanteria
apen saamia pihoja anopin asettamia
veljen veistopenkeriä sisaren siniketoja
pole jalka portahalle siirrä sintsin siltaselle
astu sintsiä simaista siitä siirräite sisähän
alle kuulun kurkihirren alle kaunihin katoksen

jo täällä tämänki talven jopa mennehen kesosen 
silta soitti sorsanluinen sillallista seisojaista
laki kultainen kumisi laen alla astujaista
ikkunat iloittelihe ikkunaisten istujaista

jo täällä tämänki talven jopa mennehen kesosen
kääkäset käkertelihe sormuskättä sulkijaista
kynnykset kykertelihe hienohelman hempujaista
ovet aina aukieli ovellista aukojaista

jo täällä tämänki talven jopa mennehen kesosen
perin pirtti pyörähteli pirtillistä pyyhkijäistä
sintsinen sijoittelihe sintsillistä siivojaista
vajaset vasertelihe vajallista vastakättä

jo täällä tämänki talven jopa mennehen kesosen 
piha piilten kääntelihe lastun pitkän poimijaista
aittaset alentelihe aitallista astujaista
orret notkui parret painui nuoren vaimon vaattehia

jo täällä tämänki talven jopa mennehen kesosen
kujaset kukertelihe kujallista kulkijaista
lääväset lähentelihe läävällistä läänijäistä
tanhuaiset taantelihe tanhuallista tavia

jo täällä tämänki päivän jopa päivän eilisenki
aioin ammoi aikalehmä aamuvihkon antajaista
hevoisvarsa hirnakoitsi heinävihkon heittäjäistä
kaikerti kevätkaritsa palasen parantajaista

jo täällä tämänki päivän jopa päivän eilisenki
vanhat istui ikkunoissa lapset raittoi rantasilla
naiset seisoi seinuksilla pojat porstuan ovilla
nuoren vaimon varronnassa morsiamen vuotannassa

terve nyt piha täysinesi ulkoinen urohinesi
terve vaja täysinesi vaja vierahaisinesi
terve sintsi täysinesi tuohikatto kansoinesi
terve pirtti täysinesi satalauta lapsinesi
terve kuu terve kuningas terve nuori nuoekansa
ei ole tässä ennen ollut eipä ennen eikä eilen
tämän joukon juoleutta tämän kansan kauneutta

sulho viljon veljyeni pura pois punaiset paikat
sivalluta silkkiverhot näytä tuota näätäistäsi
viisin vuosin käytyäsi kaheksin katseltuasi

tokko toit kenen käkesit käkesit käkösen tuoa
maalta valkean valita vesiltä verevän saa'a

jo tuon näen kyselemättä arvoan anelematta
toit käkösen tullessasi sinisotkan suojassasi
vihannimman virven latvan vihannasta virviköstä
tuorehimman tuomen lehvän tuorehesta tuomikosta

olipa lapsi lattialla lausui lapsi lattialta
voi veikko mitä vetelet tervaskannon kauneutta
tervapuolikon pituutta kerinkannan korkeutta

kutti kutti sulho rukka tuota toivotit ikäsi
sanoit saavasi sataisen tuovasi tuhannen neien
jo saitki hyvän sataisen  tuon tuhannen tuppeloisen
sait kuin suolta suovariksen aialta ajoharakan
pellolta pelotuslinnun mustan linnun mullokselta

mitä lie ikänsä tehnyt kuta mennehen kesosen
kun ei kinnasta kutonut saanut sukkoa su'unki
tyhjänä tuli tupahan annitoinna appelahan
hiiret kopsassa kopasi hörppäkorvat lippahassa

lokka luopuisa emäntä kalevatar vaimo kaunis
kuuli kummaisen tarinan sanan virkkoi noin nimesi
mitä lausuit lapsi kurja kuta kunnotoin latelit
muista kummat kuulukohon häväistykset häälyköhön
eipä tästä neitosesta ei tämän talon väestä

jo sanoit pahan sanasen sanan kehnon kertaelit
suusta yötisen vasikan päästä pennun päiväkunnan
hyvän on sulho neien saanut tuonut maalta maan
parahan
on kuin puola puolikypsi kuin on mansikka mäellä
tahi kuin käkönen puussa pieni lintu pihlajassa
koivussa koreasulka valorinta vaahteressa

oisi ei saanut saksastana tavannut viron takoa
tämän neitosen soreutta tämän allin armautta
tämän kasvon kauneutta tämän muo'on muhkeutta
käsivarren valkeutta kaulan hoikan kaarevuutta

eikä neiti tyhjin tullut oli turkit tuotavana
vaipat vasta saatavana ja verat ve'ettävänä

paljo on tällä neitosella oman värttinän väkeä
oman kehrän kiertämätä oman hyppisen hyveä
vaattehia valkehia talvisotkun suorimia
kevätpäivän valkomia kesäkuien kuivomia
hyvät hurstit huilahuset päänalaiset pällähykset
sivallukset silkkihuivit vilahukset villavaipat

hyvä mutso kaunis mutso mutso valkeanverevä
hyvinpä ko'issa kuuluit tyttönä ison ko'issa
hyvin kuulu kuun ikäsi miniänä miehelässä

elä huolelle rupea elä huoli huolehtia
ei sinua suolle viety ojavarrelle otettu
viety on viljamättähältä viety vielä viljemmälle
otettu oluttuvilta oluemmille otettu
hyvä neito kaunis mutso tuotapa kysyn sinulta
näitkö tänne tullessasi kekoja keräperiä
näsäpäitä närttehiä ne kaikki tämän talosen
tämän sulhon kyntämiä kyntämiä kylvämiä

neitokainen nuorukainen tuota nyt sanon sinulle
kun tunsit talohon tulla niin tunne talossa olla
hyvä tääll' on mutson olla kaunis kasvoa miniän
piossasi piimäpytty voivatinen vallassasi

hyvä täss' on neien olla kaunis kasvoa kanasen
täss' on laajat saunan lauat ja leveät pirtin lautsat
isännät isosi verrat emännät emosi verrat
pojat onpi veikon verrat tyttäret sisaren verrat

kun sinun himo tulevi noita mielesi tekevi
ison saamia kaloja veljen pyitä pyytämiä
niin elä kysy ky'yltä eläkä ano apelta
kysy suorin sulholtasi toimittele tuojaltasi
ei ole sitä metsässä jalan neljän juoksijata
eikä ilman lintusia kahen siiven siukovia
ei vielä ve'essäkänä kalaparvea parasta
kuta sinun ei saaja saane saaja saane tuoja tuone

hyvä täss' on neien olla kaunis kasvoa kanasen
ei ole kiirettä kivelle eikä huolta huhmarelle
vesi tässä vehnät jauhoi koski kuohutti rukihit
aalto astiat pesevi meren vaahti valkaisevi

ohoh kullaista kyläistä maan parasta paikaistani
nurmet alla pellot päällä keskellä kylä välillä
kylän alla armas ranta rannassa rakas vetonen
se sopivi sorsan uia vesilinnun vieretellä

siitä joukko juotettihin syötettihin juotettihin
liioilla lihamuruilla kaunihilla kakkaroilla
olu'illa ohraisilla viertehillä vehnäisillä

olipa kystä kyllin syöä kyllin syöä kyllin juoa
punaisissa purtiloissa kaunoisissa kaukaloissa
pirotella piirahia murotella voimuruja
sirotella siikasia lohkota lohikaloja
veitsellä hope'isella kuraksella kultaisella

olut juoksi ostamatoin mesi markoin maksamatoin
oluoinen orren päästä sima vaarnojen sisästä
olut huulten huuhtimeksi mesi mielten kääntimeksi

kukapa tuossa kukkujaksi lailliseksi laulajaksi
vaka vanha väinämöinen laulaja iän-ikuinen
itse laululle rupesi töille virtten työntelihe
sanovi sanalla tuolla lausui tuolla lausehella
veli kullat veitoseni suulliset sanalliseni
kielelliset kumppalini kuulkottenpa kuin sanelen
harvoin on hanhet suutasusten sisarukset silmätysten
harvoin veikot vieretysten emon lapset laiatusten
näillä raukoilla rajoilla poloisilla pohjan mailla

niin joko laululle lähemme töille virtten
työnteleimme
laulanta runoilla töitä kukunta kevätkäellä
painanta sinettärillä kuonta kankahattarilla

laulavat lapinki lapset heinäkengät heittelevät
hirven harvoilta lihoilta peuran pienen pallehilta
niin miks' en minäki laula miks' ei laula meiän lapset
ruoalta rukihiselta suulta suurukselliselta

laulavat lapinki lapset heläjävät heinäkengät
vesimaljan juotuansa petäjäisen purtuansa
niin miks' en minäki laula miks' ei laula meiän lapset
juomilta jyvällisiltä olu'ilta ohraisilta

laulavat lapinki lapset heläjävät heinäkengät
nokisilta nuotioilta hiilisiltä hiertimiltä
niin miks' en minäki laula miks' ei laula meiän lapset
alta kuulun kurkihirren alta kaunihin katoksen

hyväpä täss' on miesten olla armas naistenki asua
olutpuolikon povella mesitiinun tienohilla
sivullamme siikasalmet luonamme lohiapajat
joist' ei syöen syömät puutu juoen juomiset vähene

hyväpä täss' on miesten olla armas naistenki elellä
ei tässä surulla syöä ei eletä huolen kanssa
tässä syöähän surutta eletähän huoletoinna
iällä tämän isännän elinajalla emännän

kumman tässä ensin kiitän isännänkö vai emännän
ainap' entiset urohot ensin kiittivät isännän
ku on suolta suojan saanut ko'in korvesta kokenut
tyvin tuonut tyyskät männyt latvoin lansatut petäjät
pannut paikalle hyvälle asettanut ankaralle
suuriksi sukutuviksi kaunihiksi kartanoiksi
salvannut salosta seinät hirret hirmulta mäeltä
ruotehet rome'ikolta malat marjakankahalta
tuohet tuomivaaran päältä sammalet sulilta soilta

tupa on tehty tesmällensä suoja pantu paikallensa
sata oli miestä salvaimella tuhat oli tuvan katolla
tehessä tätä tupoa laaittaissa lattiata

jopa vaan tämän isännän saaessa tätä tupoa
mont' on tukka tuulta nähnyt hivus säätä hirveätä
use'in hyvän isännän jäänyt on kinnasta kivelle
hattua havun selälle suohon sukkoa vajonnut

use'in hyvä isäntä aivan aika-huomenessa
ennen muien nousematta kyläkunnan kuulematta
nousnut on nuotiotulelta havannut havumajoilta
havu päänsä harjaellut kaste pesnyt sirkut silmät

siitäpä hyvä isäntä saapi tuttua tupahan
lautsantäyen laulajoita ikkunat iloitsijoita
siltalauat lausujoita karsinat karehtijoita
seinävieret seisojia aitovieret astujia
pihat pitkin kulkijoita maat ristin matelijoita

isännän esinnä kiitin siitä ehtoisen emännän
ruokien rakentamasta pitkän pöyän täyttämästä

hänpä leipoi leivät paksut suuret talkkunat taputti
käpe'illä kämmenillä kyperillä kymmenillä
nosti leivät leppeästi syötti vierahat välehen
liioilla sianlihoilla kohokuori-kokkaroilla
terät vieri veitsistämme päät putosi puukoistamme
lohen päitä lohkoessa hauin päitä halkoessa

use'in hyvä emäntä tuo tarkka taloinen vaimo
kuullut on kukotta nousta kanan lapsetta karata
näitä häitä hankittaissa teoksia tehtäessä
hiivoja rakettaessa olosia pantaessa

hyvin on hyvä emäntä tuo tarkka taloinen vaimo
osannut oluet panna makujuoman juoksutella
iuista imeltyneistä make'ista maltahista
joit' ei puulla puuhaellut korennolla koukkaellut
vaanpa kourilla kohenti käsivarsin käännytteli
saunassa savuttomassa la'aistuilla lautehilla

eipä tuo hyvä emäntä tuo tarkka taloinen vaimo
laske iskulle ituja päästä maalle maltahia
käypi saunassa use'in syänyöllä yksinänsä
ei huoli susia surra pelätä metsän petoja

jopa nyt emännän kiitin vuotas kiitän patvaskani
ken on pantu patvaskaksi ken otettu oppahaksi
kylän paras patvaskana kylän onni oppahana

onpa meiän patvaskalla päällä haahen haljakkainen
se on kaita kainalosta soma suolien kohasta

onpa meiän patvaskalla onpa kauhtana kapoinen
helmat hietoa vetävi takapuolet tanteria

vähän paitoa näkyvi pikkaraisen pilkottavi
on kuin kuuttaren kutoma tinarinnan riukuttama

onpa meiän patvaskalla vyöllä ussakka utuinen
päivän tyttären kutoma kirjokynnen kirjoittama
ajalla tulettomalla tulen tietämättömällä

onpa meiän patvaskalla silkkiset sukat jalassa
silkkiset sukan sitehet säteriset säärinauhat
jotk' on kullalla ku'ottu hopealla huoliteltu

onpa meiän patvaskalla saksan kengät kelvolliset
kuni joutsenet joella vesiteiret vieremillä
tahi hanhuet havulla muuttolinnut murrikolla

onpa meiän patvaskalla kutrit kullansuortuvaiset
parta kullanpalmikkoinen päässä pystyinen kypäri
puhki pilvien puhuja läpi metsän läiköttäjä
jot' ei saatane sataisin tuotane tuhansin markoin

jo nyt kiitin patvaskani vuotas kiitän saajanaisen
mist' on saatu saajanainen kust' otettu onnellinen

tuolt' on saatu saajanainen tuolt' otettu onnellinen
takoa tanikan linnan uuen linnan ulkopuolta

eipä vielä sieltäkänä ei perän pereäkänä
tuolt' on saatu saajanainen tuolt' otettu onnellinen
vienan pääliltä vesiltä ulapoilta auke'ilta

eipä vielä sieltäkänä ei perän pereäkänä
kasvoi maalla mansimarja punapuola kankahalla
pellolla heleä heinä kukka kultainen aholla
siit' on saatu saajanainen siit' otettu onnellinen

saajanaisen suu somainen kuni suomen sukkulainen
saajanaisen sirkut silmät kuni tähet taivahalla
saajanaisen kuulut kulmat kuni kuu meren-ylinen

onpa meiän saajanaisen kaula kullankiehkuroissa
pää kullanvipalehissa käet kullankäärilöissä
sormet kullansormuksissa korvat kullanhelmilöissä
kulmat kullansolmuloissa silmäripset simpsukoissa

luulin kuun kumottavaksi kuu kumotti kultasolki
luulin päivän paistavaksi kun sen paistoi paian kaulus
luulin laivan läikkyväksi kun sen läikkyi lakki päässä

jopa kiitin saajanaisen annas katson kaiken kansan
onko kansa kaunihina väki vanha vänkeänä
sekä nuoriso somana koko joukko juoleana

jopa katsoin kaiken kansan ehkä tiesin ennoltaki
eip' ole tässä ennen ollut eikä varsin vasta liene
tämän joukon juoleutta tämän kansan kauneutta
väen vanhan vänkeyttä väen nuorison somuutta
kaikk' on kansa haljakassa kuni metsä huutehessa
alta on kuin aamurusko päältä on kuin päivänkoite

huokeat oli hopeat löyhät kullat kutsuloilla
rahataskut tanterilla rahakukkarot kujilla
näillä kutsuvierahilla kutsuloille kunniaksi

vaka vanha väinämöinen virren ponsi polvu'inen
siitä siirtihe rekehen lähtevi kohin kotia
laulelevi virsissänsä laulelevi taitelevi
lauloi virren lauloi toisen  virrelläpä kolmannella
kilahti jalas kivehen tarttui kapla kannon päähän
rikkoihe reki runolta jalas taittui laulajalta
kapla poikki paukahutti laiat irti loskahutti

sanoi vanha väinämöinen itse virkkoi noin nimesi
onko tässä nuorisossa kansassa kasuavassa
vaiko tässä vanhalassa väessä vähenevässä
kenpä tuonelle kävisi lähtisi manan majoille
toisi tuonelta orasen vääntiän manan väeltä
reki uusi laatiani korjanen kohentoani

sekä nuoremmat sanovi jotta vanhat vastoavi
ei ole tässä nuorisossa eikä varsin vanhastossa
koko suuressa su'ussa niin urosta urheata
jotta tuonelle menisi lähtisi manan majoille
toisi tuonelta orasen vääntiän manan majoilta
reki uusi laatiasi korjanen kohentoasi

silloin vanha väinämöinen laulaja iän-ikuinen
läksi toiste tuonelahan matkasi manan majoille
toi orasen tuonelasta vääntiän manan majoilta

siitä vanha väinämöinen laulavi salon sinisen
salohon tasaisen tammen sekä pihlajan pätevän
ne kohenti korjaksensa painalti jalaksiksensa
niistä katsoi kaplaksia sekä väänti vempeleitä
sai korjan kohennetuksi re'en uuen laaituksi
pisti varsan valjahisin ruskean re'en etehen
itse istuihe rekehen laskettihe laitiohon
vitsattaki virkku juoksi helmin lyömättä hevonen
entisille appehille taanoisille suuruksille
saattoi vanhan väinämöisen laulajan iän-ikuisen
oman uksen aukomille oman kynnyksen etehen
