kullervo kalervon poika sinisukka äijön lapsi
sai tuosta elelemähän alla varjon vanhempien
ei saanut älyämähän miehen mieltä ottamahan
kun oli kaltoin kasvateltu tuhmin lasna tuuiteltu
luona kalton kasvattajan tuon on tuhman tuuittajan

poika työlle työnteleikse raaolle rakenteleikse
kaalasi kalastamahan nuotan suuren souantahan
itse tuossa noin sanovi airo käessä arvelevi
veänkö väen mukahan souan tarmoni takoa
vai veän asun mukahan souan tarpehen takoa

perimies perältä lausui sanan virkkoi noin nimesi
jos veät väen mukahan souat tarmosi takoa
et vetäne purtta poikki etkä hankoja hajalle

kullervo kalervon poika veälti väen mukahan
souti tarmonsa takoa souti poikki puiset hangat
katajaiset kaaret katkoi venon haapaisen hajotti

sai kalervo katsomahan sanan virkkoi noin nimesi
ei sinusta soutajaksi souit poikki puiset hangat
katajaiset kaaret katkoit koko haapion hajotit
mene nuotan tarvontahan lienet tarpoja parempi

kullervo kalervon poika meni nuotan tarvontahan
itse tuossa tarpoimelta sanan virkkoi noin nimesi
tarvonko olan takoa pane miehuuen nojassa

kullervo kalervon poika tarpaisi olan takoa
pani miehuuen nojassa ve'en velliksi seotti
tarpoi nuotan tappuroiksi kalat liivaksi litsotti

sai kalervo katsomahan sanan virkkoi noin nimesi
ei sinusta tarpojaksi tarvoit nuotan tappuroiksi
ruumeniksi pullot rouhit selykset paloin paloitit
lähe viemähän vetoja maarahoja maksamahan
lienet matkassa parempi taipalella taitavampi

kullervo kalervon poika sinisukka äijön lapsi
hivus keltainen korea kengän kauto kaunokainen
läksi viemähän vetoja maajyviä maksamahan

vietyä vetoperänsä maajyväset maksettua
rekehensä reutoaikse kohennaikse korjahansa
alkoi kulkea kotihin matkata omille maille

ajoa järyttelevi matkoansa mittelevi
noilla väinön kankahilla ammoin raatuilla ahoilla

neiti vastahan tulevi hivus kulta hiihtelevi
noilla väinön kankahilla ammoin raatuilla ahoilla

kullervo kalervon poika jo tuossa piättelevi
alkoi neittä haastatella haastatella houkutella
nouse neito korjahani taaksi maata taljoilleni

neiti suksilta sanovi hiihtimiltä hiioavi
surma sulle korjahasi tauti taaksi taljoillesi

kullervo kalervon poika sinisukka äijön lapsi
iski virkkua vitsalla helähytti helmivyöllä
virkku juoksi matka joutui tie vieri reki rasasi
ajoa järyttelevi matkoansa mittelevi
selvällä meren selällä ulapalla aukealla

neiti vastahan tulevi kautokenkä kaaloavi
selvällä meren selällä ulapalla aukealla

kullervo kalervon poika hevoista piättelevi
suutansa sovittelevi sanojansa säätelevi
tule korjahan korea maan valio matkoihini

neiti vastahan sanovi kautokenkä kalkuttavi
tuomi sulle korjahasi manalainen  matkoihisi

kullervo kalervon poika sinisukka äijön lapsi
iski virkkua vitsalla helähytti helmivyöllä
virkku juoksi matka joutui reki vieri ti lyheni
ajavi karettelevi matkoansa mittelevi
noilla pohjan kankahilla lapin laajoilla rajoilla

neiti vastahan tulevi tinrinta riioavi
noilla pohjan kankahilla lapin laajoilla rajoilla

kullervo kalervon poika hevoistansa hillitsevi
suutansa sovittelevi sanojansa säätelevi
käy neito rekoseheni armas alle vilttieni
syömähän omeniani puremahan päähkeniä

neiti vastahan sanovi tinarinta riuskuttavi
sylen kehno kelkkahasi retkale rekosehesi
vilu on olla viltin alla kolkko korjassa eleä

kullervo kalervon poika sinisukka äijön lapsi
koppoi neion korjahansa reualti rekosehensa
asetteli taljoillensa alle viltin vieritteli

neiti tuossa noin sanovi tinarinta riitelevi
päästä pois minua tästä laske lasta vallallensa
kunnotointa kuulemasta pahalaista palvomasta
tahi potkin pohjan puhki levittelen liistehesi
korjasi pilastehiksi rämäksi re'en retukan

kullervo kalervon poika sinisukka äijön lapsi
aukaise rahaisen arkun kimahutti kirjakannen
näytteli hope'itansa verkaliuskoja levitti
kultasuita sukkasia vöitänsä hopeapäitä

verat veivät neien mielen raha muuttui morsiamen
hopea hukuttelevi kulta kuihauttelevi

kullervo kalervon poika sinisukka äijön lapsi
tuossa neittä mairotteli kuihutteli kutkutteli
käsi orosen ohjaksissa toinen neitosen nisoissa
siinä neitosen kisasi tinarinnan riu'utteli
alla vaipan vaskikirjan päällä taljan taplikkaisen

jo antoi jumala aamun toi jumala toisen päivän
niin neiti sanoiksi virkki kysytteli lausutteli
mist' olet sinä sukuisin kusta rohkea rotuisin
lienet suurtaki sukua isoa isän aloa

kullervo kalervon poika sanan virkkoi noin nimesi
en ole sukua suurta enkä suurta enkä pientä
olen kerran keskimäistä kalervon katala tyttö
tyhjä tyttö tuiretuinen lapsi kehjo keiretyinen

ennen lasna ollessani emon ehtoisen eloilla
läksin marjahan metsälle alle vaaran vaapukkahan
poimin maalta mansikoita alta vaaran vaapukoita
poimin päivän yön lepäsin poimin päivän poimin toisen
päivälläpä kolmannella en tiennyt kotihin tietä
tiehyt metsähän veteli ura saatteli salolle

siinä istuin jotta itkin itkin päivän jotta toisen
päivänäpä kolmantena nousin suurelle mäelle
korkealle kukkulalle tuossa huusin hoilaelin
salot vastahan saneli kankahat kajahtelivat
'elä huua hullu tyttö elä mieltöin melua
ei se kuulu kumminkana ei kuulu kotihin huuto'

päivän päästä kolmen neljän viien kuuen viimeistäki
kohennihin kuolemahan heitihin katoamahan
enkä kuollut kuitenkana en mä kalkinen kaonnut

oisin kuollut kurja raukka oisin katkennut katala
äsken tuosta toisna vuonna kohta kolmanna kesänä
oisin heinänä helynnyt kukoistellut kukkapäänä
maassa marjana hyvänä punaisena puolukkana
nämä kummat kuulematta haikeat havaitsematta

sai toki sanoneheksi kerran kertoelleheksi
heti repsahti re'estä siitä juoksihe jokehen
kosken kuohu'un kovahan palavahan pyörtehesen
siihen surmansa sukesi kuolemansa kohtaeli
löyti turvan tuonelassa armon aaltojen seassa

kullervo kalervon poika pyyhältihe korjastansa
alkoi itkeä isosti valitella vaikeasti
voi poloinen päiviäni voipa kurja kummiani
kun pi'in sisarueni turmelin emoni tuoman
voi isoni voi emoni vi on valtavanhempani
minnekä minua loitte kunne kannoitte katalan
parempi olisin ollut syntymättä kasvamatta
ilmahan sikeämättä maalle tälle täytymättä
eikä surma suorin tehnyt tauti oike'in osannut
kun ei tappanut minua kaottanut kaksiöisnä

veitsen länkensä levitti rauoin rahnoi rahkehensa
hyppäsi hyvän selälle hyvän laukin lautasille
ajavi palasen maata pikkaraisen piirreltävi
päätyvi ison pihoille oman taaton tanterelle

emo päätyvi pihalle oi emoni kantajani
kun oisit emo kuluni synnyteltäissä minua
pannut saunahan savua lyönyt saunan salpa päälle
tukahuttanut savuhun kaottanut kaksiöisnä
vienyt hurstilla vetehen upotellut uutimella
luonut tuutusen tulehen liekun lietehen sysännyt

oisiko kylä kysynyt 'missä tuutunen tuvasta
mitä sauna salpa päällä' sinä oisit vastannunna
'tuutusen tulessa poltin liekun liesivalkeassa
saunassa te'in ituja ma'ustelin maltahia'

emo ennätti kysyä vanhempansa tutkaella
mi sinulla poikaseni mikä kumma kuulumassa
on kuin tuonelta tulisit manalalta matkoaisit

kullervo kalervon poika sanan virkkoi noin nimesi
jo nyt on kummat kuulununna turmiot tapahtununna
kun pi'in oman sisaren turmelin emoni tuoman

tulin viennästä vetojen maarahojen maksannasta
päätyi neito vastahani mie tuota kisauttelin
se oli sisarueni se oman emoni lapsi

se jo surmansa sukesi kuolemansa kohtaeli
kosken kuohu'un kovahan palavahan pyörtehesen
itse en nyt tieäkänä arvoa älyäkänä
kunne surmani sukean kunne kurja kuoletaime
suuhun ulvovan sutosen karhun kiljuvan kitahan
vainko vatsahan valahan meren hauin hampahisin

emo tuon sanoiksi virkki ellös menkö poikaseni
suuhun ulvovan sutosen karhun kiljuvan kitahan
eläkä vatsahan valahan hauin hirmun hampahisin
onpa suurta suomen nientä sankoa savon rajoa
piillä miehen pillojansa hävetä pahoja töitä
piillä vuotta viisi kuusi ynnähän yheksän vuotta
kunnes aika armon tuopi vuoet huolen huojentavi

kullervo kalervon poika sanan virkkoi noin nimesi
enkä lähe piilemähän en paha pakenemahan
lähen surman suun esille kalman kartanon oville
suurille sotasijoille miesten tappotanterille
viel' on unto oikeana mies katala kaatamatta
kostamatta taaton kohlut maammon mahlat maksamatta
muistamatta muutki vaivat itseni hyvin-piännät
