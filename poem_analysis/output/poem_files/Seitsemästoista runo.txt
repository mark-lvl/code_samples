vaka vanha väinämöinen kun ei saanunna sanoja
tuolta tuonelan ko'ista manalan ikimajoista
ain' yhä ajattelevi pitkin päätänsä pitävi
mistäpä sanoja saisi loisi lempiluottehia

paimen vastahan tulevi hänpä tuon sanoiksi virkki
saat tuolta sata sanoa tuhat virren tutkelmusta
suusta antero vipusen vatsasta varaväkevän
vaan se on sinne mentävätä polku poimeteltavata
ei ole matkoa hyveä ei aivan pahintakana
yks' on juoni juostaksesi naisten neulojen neniä
tuosta toinen käyäksesi miehen miekan tutkaimia
kolmas koikutellaksesi uron tapparan teriä

vaka vanha väinämöinen toki mietti mennäksensä
painuvi sepän pajahan sanovi sanalla tuolla
ohoh seppo ilmarinen taos rautaiset talukset
tao rautarukkahiset paita rautainen rakenna
laai rautainen korento teräksinen tienaellos
pane syämehen teräkset veä päälle melto rauta
lähen saamahan sanoja ongelmoita ottamahan
vatsasta varaväkevän suusta antero vipusen

se on seppo ilmarinen sanan virkkoi noin nimesi
viikon on vipunen kuollut kauan antero kaonnut
vipunsa virittämästä ahtamasta ansatiensä
et sieltä sanoa saane et sanoa puoltakana

vaka vanha väinämöinen toki läksi ei totellut
astui päivän helkytteli naisten neulojen neniä
astui toisen torkutteli miesten miekan tutkaimia
kolmannenki koikutteli uron tapparan teriä

itse virsikäs vipunen mies vanha varaväkevä
tuo viruvi virsinensä luottehinensa lojuvi
haapa kasvoi hartioilla koivu kulmilla yleni
leppä leukaluun nenässä pajupehko parran päällä
otsalla oravikuusi havuhonka hampahilla

jo tulevi väinämöinen veti miekan riitsi rauan
huotrasta huveksisesta vyöstä vennon-selkäisestä
kaatoi haavan hartioilta koivut kulmilta kukisti
leuoilta lepät leveät pajupehkot parran päältä
otsalta oravikuuset havuhongat hampahilta

syösti rautaisen korennon suuhun antero vipusen
ikenihin irjuvihin leukoihin lotisevihin
sanan virkkoi noin nimesi nouse pois inehmon orja
maan alla makoamasta viikon unta ottamasta

tuop' on virsikäs vipunen heti herkesi unesta
tunsi koskevan kovasti kipeästi kiusaisevan
puri rautaisen korennon puri päältä mellon rauan
ei tiennyt terästä purra ei syöä syäntä rauan

tuossa vanhan väinämöisen suun ohella seistessänsä
jalka toinen torkahtavi vasen jalka vaapahtavi
suuhun antero vipusen leukaluulle luikahutti
heti virsikäs vipunen avoi suunsa suuremmaksi
leukapielensä levitti  nieli miehen miekkoinensa
kulahutti kulkkuhunsa tuon on vanhan väinämöisen

siinä virsikäs vipunen itse tuon sanoiksi virkki
jo olen jotaki syönyt syönyt uuhta syönyt vuohta
syönyt lehmeä mahoa syönyt karjua sikoa
en ole vielä mointa syönyt en tämän palan makuista

itse vanha väinämöinen hänpä tuon sanoiksi virkki
jo taisi tuhoni tulla hätäpäivä hämmenteä
tämän hiien hinkalossa tämän kalman karsinassa

arvelee ajattelevi miten olla kuin eleä
veitsi on vyöllä väinämöisen pää visainen veitsessänsä
tuosta hän teki venosen teki tieolla venosen
soutelevi luitelevi suolen päästä suolen päähän
souteli joka solukan joka supun suikerteli

vanha virsikäs vipunen ei tuosta totella ollut
silloin vanha väinämöinen löihen itsensä sepoksi
rakentihe rautioksi painoi paitansa pajaksi
hiat paian palkehiksi turkkinsa tuhottimeksi
housut hormiksi rakenti sukat hormin suulliseksi
polvensa alasimeksi vasaraksi kyynäspäänsä

takoa taputtelevi lyöä lynnähyttelevi
takoi yön lepeämättä päivän pouahuttamatta
vatsassa varaväkevän mahtipontisen povessa

silloin virsikäs vipunen itse tuon sanoiksi virkki
mi sinä lienet miehiäsi ja kuka urohiasi
jo olen syönyt saan urosta tuhonnut tuhannen miestä
enpä liene mointa syönyt syet suuhuni tulevat
kekälehet kielelleni rauan kuonat kulkkuhuni

lähe nyt kumma kulkemahan maan paha
pakenemahan
ennenkuin emosi etsin haen valtavanhempasi
jos sanon minä emolle virkan vierin vanhemmalle
enemp' on emolla työtä vaiva suuri vanhemmalla
kun poika pahoin tekevi lapsi anke'in asuvi

en nyt tuota tunnekana enkä arvoa alusta
mist' olet hiisi hingannunna kusta turma tänne tullut
puremahan jäytämähän syömähän kaluamahan
oletko tauti luojan luoma surma säätämä jumalan
vain olet teko tekemä toisen tuoma toisen luoma
pantu tänne palkan eestä rakettu rahan nenästä

ollet tauti luojan luoma surma säätämä jumalan
niinp' on luome luojahani heitäime jumalahani
ei herra hyveä heitä luoja ei kaunista kaota

kun lienet teko tekemä pulma toisen pungastama
kyllä saan sukusi tietä löyän synnyntäsijasi

tuolta ennen pulmat puuttui tuolta taikeat tapahtui
tietomiesten tienohilta laulumiesten laitumilta
konnien kotisijoilta taikurien tanterilta
tuolta kalman kankahilta maasta manteren sisästä
miehen kuollehen ko'ista kaonnehen kartanosta
mullista muhajavista maista liikuteltavista
somerilta pyöriviltä hiekoilta heliseviltä
notkoilta noroperiltä soilta sammalettomilta
here'istä hettehistä läikkyvistä lähtehistä
metsän hiien hinkalosta viien vuoren vinkalosta
vaaran vaskisen laelta kuparisen kukkulalta
kuusista kuhisevista hongista hohisevista
latvasta lahon petäjän mätäpäistä mäntylöistä
revon rääyntäsijoilta hirven hiihtokankahilta
kontion kivikolosta karhun louhikammiosta
pohjan pitkästä perästä lapin maasta laukeasta
ahoilta vesattomilta mailta kyntämättömiltä
suurilta sotakeoilta miehentappo-tanterilta
ruohoista rohisevista hurmehista huuruvista
suurilta meren seliltä ulapoilta auke'ilta
meren mustista mu'ista tuhannen sylen syvästä
virroista vihisevistä palavoista pyörtehistä
rutjan koskesta kovasta ve'en vankan vääntehestä
takaisesta taivahasta poutapilvien periltä
ahavan ajeloteiltä tuulen tuutimasijoilta

sieltäkö sinäki puutuit sieltä taikea tapahuit
syämehen syyttömähän vatsahan viattomahan
syömähän kaluamahan puremahan louhtamahan

himmene nyt hiien hurtta raukea manalan rakki
lähe pois kohusta konna maan kamala maksoistani
syömästä syänkäpyä pernoani pehkomasta
vatsoa vanuttamasta keuhkoloita kiertämästä
napoa navertamasta ohimoita ottamasta
selkäluita luistamasta sivuja sivertämästä

jos ei minussa miestä liene niin panen parempiani
tämän pulman purkajaksi kauhean kaottajaksi

nostan maasta mannun eukot pellosta peri-isännät
kaikki maasta miekkamiehet hiekasta hevoisurohot
väekseni voimakseni tuekseni turvakseni
tässä työssä työlähässä tässä tuskassa kovassa

kun ei tuostana totelle vääjänne väheäkänä
nouse metsä miehinesi katajikko kansoinesi
petäikkö perehinesi umpilampi lapsinesi
sata miestä miekallista tuhat rauaista urosta
tätä hiittä hieromahan juutasta rutistamahan

kun ei tuostana totelle vääjänne väheäkänä
nouse veestä veen emäntä sinilakki lainehista
hienohelma hettehestä puhasmuotoinen muasta
väeksi vähän urohon miehen pienen miehuueksi
jottei minua syyttä syöä eikä tauitta tapeta

kun ei tuostana totelle vääjänne väheäkänä
kave eukko luonnon tytti kave kultainen korea
jok' olet vanhin vaimoloita ensin emä itselöitä
käy nyt tuskat tuntemahan hätäpäivät häätämähän
tämä jakso jaksamahan puutunnainen purkamahan

ja kun ei sitä totelle välttäne väheäkänä
ukko taivahan-napainen remupilven-reunahinen
tule tänne tarvittaissa ajaite anottaessa
työt kehnot kerittämähän rikkonaiset riisumahan
miekalla tuliterällä säilällä säkehisellä

lähe nyt kumma kulkemahan maan paha
pakenemahan
ei täällä sinun sijoa sijankana tarpehella
muunne muuttaos majasi etemmä elosijasi
isäntäsi istumille emäntäsi astumille

sitte sinne tultuasi matkan päähän päästyäsi
tekijäsi tienohille laittajasi laitumille
laai tunnus tultuasi salamerkki saatuasi
jyskä kuin ukon jyrynen välkä kuin tulen välähys
potkaise pihalta portti laske lauta ikkunasta
siitä siirräite sisähän lennä tupruna tupahan
ota kiinni kinterestä kai'immasta kantapäästä
isännät perisopesta emännät ovisopesta
isännältä silmä kaiva emännältä pää murota
sormet koukkuhun koverra väännä päätä väärällehen

jos siitä vähän tulisi lennä kukkona kujalle
kanan lasna kartanolle rinnoin rikkatunkiolle
sorra soimelta hevonen navetasta sarvinauta
sarvet sontahan sovita häntä laske lattialle
silmät käännä kellellehen niskat ruttohon rutaise

oletko tauti tuulen tuoma tuulen tuoma vuon ajama
ahavaisen antelema vilun ilman viehättämä
mene tuulen tietä myöten ahavan rekiratoja
ilman puussa istumatta lepässä lepeämättä
vaaran vaskisen laelle kuparisen kukkulalle
siellä tuulen tuuitella ahavaisen akkiloia

lienet tullut taivahalta poutapilvien periltä
nouse taasen taivahalle tuonne ilmoille ylene
pilvihin pirisevihin tähtihin tärisevihin
tulena palelemahan säkehinä säikkymähän
auringon ajelemilla kuun kehyen kiertämillä

lienet vieno veen vetämä meren aaltojen ajama
niin vieno vetehen mennös alle aaltojen ajaite
mutalinnan liepehille vesiharjun hartehille
siellä aaltojen ajella ve'en synkän sylkytellä

lienet kalman kankahalta ikimennehen majoilta
toki koitellos kotia noille kalman kartanoille
multihin muhajavihin maihin liikuteltavihin
johon on kansa kaatununna väki vahva vääntynynnä

kun liet tuhma tuolta tullut metsän hiien hinkalosta
petäjäisistä pesistä honkaisista huonehista
niin sinne sinun manoan metsän hiien hinkalohon
honkaisihin huonehisin petäjäisihin pesihin
sini siellä ollaksesi kunnes lattiat lahovat
seinähirret sienettyvät laki päältä laukeavi

ja tuonne sinun manoan tuonne kehnoa kehoitan
ukkokontion kotihin akkakarhun kartanohon
notkoille noroperille soille räykymättömille
heiluvihin hettehisin läilyvihin lähtehisin
lampihin kalattomihin aivan ahvenettomihin

et siellä sijoa saane niin tuonne sinun manoan
pohjan pitkähän perähän lapin maahan laukeahan
ahoille vesattomille maille kyntämättömille
kuss' ei kuuta aurinkoa eikä päiveä iässä
siell' on onni ollaksesi lempi liehaellaksesi
hirvet on puihin hirtettynä jalot peurat jaksettuna
syöä miehen nälkähisen haukata halun-alaisen

ja tuonne sinun manoan tuonne käsken ja kehoitan
rutjan koskehen kovahan palavahan pyörtehesen
johon puut pä'in putoovat perin vierivät petäjät
tyvin syösten suuret hongat latvoin lakkapäät petäjät
ui siellä paha pakana kosken kuohuja kovia
ve'et väljät väännättele ve'et ahtahat asuile

et siellä sijoa saane niin tuonne sinun manoan
tuonen mustahan jokehen manalan ikipurohon
jost' et pääse päivinäsi selviä sinä ikänä
kun en pääsne päästämähän kerinne kerittämähän
yheksällä oinahalla yhen uuhen kantamalla
yheksällä härkäsellä yhen lehmäsen vasoilla
yheksän oron keralla yhen tamman varsasilla

josp' on kyytiä kysynet anonet ajohevoista
kyllä mä sulle kyyin laitan ja annan ajohevosen
hiiess' on hyvä hevonen punatukka tunturissa
jonka turpa tulta tuiski nenä varsin valkeata
kaikki on rautaiset kapiot teräksiset temmottimet
ne jaksaa mäkehen mennä nousta notkon penkerehen
hyvällä hypittäjällä ajajalla ankaralla

kun ei siitä kyllin liene saaos hiien hiihtoneuvot
lemmon leppäiset sivakat pahalaisen paksu sauva
joilla hiihät hiien maita lemmon lehtoja samoat
hilpotellen hiien maita pahan maita paipotellen
kivi on tiellä poikkipuolin se poikki porahtakohon
hako tiellä pitkin puolin tuo kaheksi katketkohon
uros tiellä pystyn puolin sep' on laitahan lähetä

lähe nyt liika liikkumahan mies paha pakenemahan
ennen päivän nousemista koi-jumalan koittamista
auringon ylenemistä kukon äänen kuulumista
nyt on liian liikeaika ja pahan pakenoaika
kuutamainen kulkeasi valkea vaeltoasi

kun et vääjänne välehen eronne emotoin rakki
saan minä kokolta kourat veren juojalta vekarat
linnulta lihan pitimet havukalta haarottimet
joilla konnat kouristelen ilkeät iki asetan
pään pärisemättömäksi hengen huokumattomaksi

luopui ennen luotu lempo eksyipä emollinenki
tullessa jumalan tunnin avun luojan auetessa
etkö sie emotoin eksy luovu luonnotoin sikiä
haihu koira haltiatoin erkane emotoin rakki
tämän tunnin tutkaimella tämän kuuhuen kululla

vaka vanha väinämöinen silloin tuon sanoiksi virkki
hyvä tääll' on ollakseni armas aikaellakseni
maksat leiväksi pätevi marut maksan särpimeksi
keuhkot käypi keitokseksi rasvat ruoiksi hyviksi

asetan alasimeni syvemmin syänlihoille
painan paljani lujemmin paikoille pahemmillenki
ettet pääse päivinäsi selviä sinä ikänä
kun en saa sanoja kuulla luoa lempiluottehia
kuulla kyllältä sanoja tuhansia tutkelmoita
ei sanat salahan joua eikä luottehet lovehen
mahti ei joua maan rakohon vaikka mahtajat menevät

silloin virsikäs vipunen tuo vanha varaväkevä
jonk' oli suussa suuri tieto mahti ponnetoin povessa
aukaisi sanaisen arkun virsilippahan levitti
lauloaksensa hyviä parahia pannaksensa
noita syntyjä syviä ajan alkuluottehia
joit' ei laula kaikki lapset ymmärrä yhet urohot
tällä inhalla iällä katoavalla kannikalla

lauloi synnyt syitä myöten luottehet lomia myöten
kuinka luojansa luvalla kaikkivallan vaatimalla
itsestänsä ilma syntyi ilmasta vesi erosi
veestä manner maatelihe manterelle kasvut kaikki

lauloi kuun kuvoannasta auringon asetannasta
ilman pielten pistännästä taivosen tähytännästä

siinä virsikäs vipunen kyllä lauloi ja osasi
ei ole kuultu eikä nähty sinä ilmoisna ikänä
parempata laulajata tarkempata taitajata
suu se syyteli sanoja kieli laski lausehia
kuin on sälkö sääriänsä ratsu jalkoja jaloja

lauloi päivät pääksytysten yhytysten yöt saneli
päätyi päivä kuulemahan kuu kulta tähyämähän
aallot seisottui selällä lainehet lahen perällä
puuttui virrat vierimästä rutjan koski kuohumasta
vuotamasta vuoksen koski joki juortanin pysähtyi

siitä vanha väinämöinen kun oli sanoja kuullut
saanut kylliksi sanoja luonut lempiluottehia
rupeavi lähtemähän suusta antero vipusen
vatsasta varaväkevän mahtipontisen povesta

sanoi vanha väinämöinen oi sie antero vipunen
ava suusi suuremmaksi leukapielesi levitä
pääsisin mahasta maalle kotihini kulkemahan

siinä virsikäs vipunen itse tuon sanoiksi virkki
mont' olen syönyt monta juonut tuhonnut tuhatlukuja
moint' en vielä konsa syönyt kuin söin vanhan
väinämöisen
hyvin laait tultuasi teet paremmin kun paloat

siitä antero vipunen irvisti ikeniänsä
avoi suunsa suuremmaksi leukapielensä levitti
itse vanha väinämöinen läksi suusta suuritieon
vatsasta varaväkevän mahtipontisen povesta
luiskahtavi poies suusta kaapsahtavi kankahalle
kuin on kultainen orava tahi näätä kultarinta

läksi siitä astumahan tuli sepponsa pajahan
sanoi seppo ilmarinen joko sait sanoja kuulla
luoa lempiluottehia miten laita lasketahan
perilaita liitetähän kokkapuut kohennetahan

vaka vanha väinämöinen itse tuon sanoiksi virkki
jo nyt sain sa'an sanoja tuhansia tutkelmoita
sain sanat salasta ilmi julki luottehet lovesta

niin meni venonsa luoksi tieokkaille tehtahille
sai venonen valmihiksi laian liitto liitetyksi
peripäähyt päätetyksi kokkapuut kohotetuksi
veno syntyi veistämättä laiva lastun ottamatta
