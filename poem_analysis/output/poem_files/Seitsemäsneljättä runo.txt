se on seppo ilmarinen naista itki illat kaiket
yöt itki unettomana päivät einehettömänä
aamut aikaisin valitti huomeniset huokaeli
kun oli kuollut nuori nainen kaunis kalmahan katettu
eipä kääntynyt käessä vaskinen vasaran varsi
kuulunut pajasta kalke yhen kuuhuen kululla

sanoi seppo ilmarinen en tieä poloinen poika
miten olla kuin eleä istun yön eli makoan
äijä on yötä tunti tuhma vaivoja matala mahti

ikävät on iltaseni apeat on aamuseni
äsken yöllä äitelämpi havatessa haikeampi
ei ole iltoja ikävä ei apea aamujani
mure muita aikojani ihanaistani ikävä
apeainen armastani mure mustakulmaistani

jo vainen iällä tällä use'in minun utuisen
keskiöisissä unissa koura tyhjeä kokevi
käsi vaalivi valetta kupehelta kummaltaki

seppo naisetta elävi puolisotta vanhenevi
itki kuuta kaksi kolme niinpä kuulla neljännellä
poimi kultia mereltä hope'ita lainehilta
keräsi kekosen puita kolmekymmentä rekoista
puunsa poltti hiililöiksi hiilet ahjohon ajeli

otti noita kultiansa valitsi hope'itansa
sykysyisen uuhen verran verran talvisen jäniksen
työnti kullat kuumentohon ajoi ahjohon hopeat
pani orjat lietsomahan palkkalaiset painamahan

orjat lietsoi löyhytteli palkkalaiset painatteli
kintahattomin kätösin hatuttoman hartioisen
itse seppo ilmarinen ahjoa kohentelevi
pyyti kullaista kuvaista hope'ista morsianta

ei orjat hyvästi lietso eikä paina palkkalaiset
se on seppo ilmarinen itse löihe lietsomahan
lietsahutti kerran kaksi niin kerralla kolmannella
katsoi ahjonsa alusta lietsehensä liepehiä
mitä ahjosta ajaikse tungeikse tulisijasta

uuhi ahjosta ajaikse lähetäikse lietsehestä
karva kulta toinen vaski kolmas on hopeakarva
muut tuota ihastelevi ei ihastu ilmarinen

sanoi seppo ilmarinen se susi sinuista toivoi
toivon kullaista sopua hope'ista puolisoa

siitä seppo ilmarinen uuhen työntävi tulehen
liitti kultia lisäksi hope'ita täytteheksi
pani orjat lietsomahan palkkalaiset painamahan

orjat lietsoi löyhytteli palkkalaiset painatteli
kintahattomin kätösin hatuttoman hartioisen
itse seppo ilmarinen ahjoa kohentelevi
pyyti kullaista kuvoa hope'ista morsianta

ei orjat hyvästi lietso eikä paina palkkalaiset
se on seppo ilmarinen itse loihe lietsomahan
lietsahutti kerran kaksi niin kerralla kolmannella
katsoi ahjonsa alusta lietsehensä liepehiä
mitä ahjosta ajaikse lähetäikse lietsehestä

varsa ahjosta ajaikse lähetäikse lietsehestä
harja kulta pää hopea kaikki vaskesta kaviot
muut tuota hyvin ihastui ei ihastu ilmarinen

sanoi seppo ilmarinen se susi sinuista toivoi
toivon kullaista sopua hope'ista puolisoa

siitä seppo ilmarinen varsan työntävi tulehen
liitti kultia lisäksi hope'ita täytteheksi
pani orjat lietsomahan palkkalaiset painamahan

orjat lietsoi löyhytteli palkkalaiset painatteli
kintahattomin kätösin hatuttoman hartioisen

itse seppo ilmarinen ahjoa kohentelevi
pyyti kullaista kuvoa hope'ista morsianta

ei orjat hyvästi lietso eikä paina palkkalaiset
se on seppo ilmarinen itse loihe lietsomahan
lietsahutti kerran kaksi niin kerralla kolmannella
katsoi ahjonsa alusta lietsehensä liepehiä
mitä ahjosta ajaikse lähetäikse lietsehestä

neiti ahjosta ajaikse kultaletti lietsehestä
pää hopea kassa kulta varsi kaikki kaunokainen
muut tuota pahoin pelästyi ei pelästy ilmarinen

siitä seppo ilmarinen takoi kullaista kuvoa
takoi yön levähtämättä päivän pouahuttamatta
jalat laati neitoselle jalat laati käet kuvasi
eipä jalka nousekana käänny käet syleilemähän

takoi korvat neiollensa eipä korvat kuulekana
niin sovitti suun sorean suun sorean sirkut silmät
saanut ei sanoa suuhun eikä silmähän suloa

sanoi seppo ilmarinen oisi tuo sorea neito
kun oisi sanallisena mielellisnä kielellisnä

saattoi siitä neitosensa utuisehen uutimehen
pehme'ille pääaloille sulkkuisille vuotehille

siitä seppo ilmarinen lämmitti kylyn utuisen
laati saunan saipuaisen vastat varpaiset varusti
vettä kolme korvollista jolla peiponen peseikse
pulmunen puhasteleikse noista kullan kuonasista

kylpi seppo kyllitellen valelihe vallotellen
neien vierehen venähti utuisehen uutimehen
teltahan teräksisehen rankisehen rautaisehen

siinä seppo ilmarinen heti yönä ensimäisnä
kyllä peitettä kysyvi vaippoja varustelevi
kahet kolmet karhuntaljat viiet kuuet villavaipat
maata kera puolisonsa tuon on kultaisen kuvansa

se oli kylki kyllä lämmin ku oli vasten vaippojansa
ku oli nuorta neittä vasten vasten kullaista kuvoa
se oli kylki kylmimässä oli hyyksi hyytymässä
meren jääksi jäätymässä kiveksi kovoamassa

sanoi seppo ilmarinen ei tämä hyvä minulle
vienen neien väinölähän väinämöiselle varaksi
polviseksi puolisoksi kainaloiseksi kanaksi

viepi neien väinölähän sitte sinne tultuansa
sanan virkkoi noin nimesi oi sie vanha väinämöinen
tuossa on sinulle tyttö neiti kaunis katsannolta
eik' ole suuri suun piolta kovin leuoilta leveä

vaka vanha väinämöinen katsahti kuvoa tuota
luopi silmät kullan päälle sanan virkkoi noin nimesi
miksi toit minulle tuota tuota kullan kummitusta

sanoi seppo ilmarinen miksi muuksi kuin hyväksi
polviseksi puolisoksi kainaloiseksi kanaksi

sanoi vanha väinämöinen oi on seppo veikkoseni
tunge neitosi tulehen tao kaikiksi kaluiksi
tahi vie venäehelle saata saksahan kuvasi
rikkahien riian naia suurien soan kosia
ei sovi minun su'ulle ei minullen itselleni
naista kullaista kosia hope'ista huolitella

siitä kielti väinämöinen epäsi suvannon sulho
kielti kansan kasvavaisen epäsi yleneväisen
kullalle kumartamasta hopealle horjumasta
sanovi sanalla tuolla lausui tuolla lausehella
elkätte pojat poloiset vasta kasvavat urohot
ollette elonkeraiset elikkä elottomatki
sinä ilmoisna ikänä kuuna kullan valkeana
naista kullaista kosiko hope'ista huolitelko
kylmän kulta kuumottavi vilun huohtavi hopea
