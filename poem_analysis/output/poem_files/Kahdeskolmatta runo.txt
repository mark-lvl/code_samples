kun oli kyllin häitä juotu pi'etty pitoja noita
häitä pohjolan tuvilla pitoja pimentolassa
sanoi pohjolan emäntä ilmariselle vävylle
mit' istut isosukuinen maan valio valvattelet
istutko ison hyvyyttä vai emonko armautta
vaiko pirtin valkeutta naimakansan kauneutta

et istu ison hyvyyttä et emosen armautta
etkä pirtin puhtautta naimakansan kauneutta
istut impesi hyvyyttä neien nuoren armautta
valvattisi valkeutta kassapääsi kauneutta

sulho viljon veljyeni vuotit viikon vuota vielä
ei ole valmis valvattisi suorinut ikisopusi
puol' on päätä palmikolla puoli palmikoitsematta

sulho viljon veljyeni vuotit viikon vuota vielä
ei ole valmis valvattisi suorinut ikisopusi
yks' on hiema hiemoitettu toinen hiemoiteltavana

sulho viljon veljyeni viikon vuotit vuota vielä
ei ole valmis valvattisi suorinut ikisopusi
vast' on jalka kengitetty toinen kengiteltävänä

sulho viljon veljyeni viikon vuotit vuota vielä
ei ole valmis valvattisi suorinut ikisopusi
käsi on toinen kinnastettu toinen kinnasteltavana

sulho viljon veljyeni viikon vuotit et väsynyt
valmis on nyt valvattisi suoriunut sorsasesi

mene jo myöten myöty neiti kanssa kaupattu kananen
jo nyt on liittosi likellä kovin läsnä lähtöaika
kun on viejä vieressäsi ottajaisesi ovilla
oro suitsia purevi reki neittä vuottelevi

oltua rakas rahoihin käpäs kättä antamahan
kiivas kihlan ottelohon sormuksen sovittelohon
ole nyt rakas rekehen kiivas kirjokorjasehen
käpäs käymähän kylähän sekä sievä lähtemähän

etpä äijän nuori neiti kahen puolesi katsellut
yli pääsi ymmärrellyt jos te'it ka'utun kaupan
iän kaiken itkemisen vuoet voikerrehtamisen
kun läksit isosi koista siirryit syntymäsijoilta
luota ehtoisen emosi kantajasi kartanoilta

mi oli sinun eleä näillä taattosi tiloilla
kasvoit kukkana kujilla ahomailla mansikkana
nousit voille vuotehelta maioille makoamasta
venymästä vehnäsille pettäjäisille pehusta
kun et voinut voita syöä silpaisit sianlihoa

ei ollut huolta ollenkana ajatusta aioinkana
annoit huolla honkasien ajatella aiaksien
surra suolla suopetäjän kangaskoivun kankahalla
itse liehuit lehtyisenä perhosena pyörähtelit
marjana emosi mailla vaapukkana vainiolla

lähet nyt talosta tästä menet toisehen talohon
toisehen emon alahan perehesen vierahasen
toisin siellä toisin täällä toisin toisessa talossa
toisin siellä torvet soivat toisin ukset ulvaisevat
toisin vierevät veräjät sanovat saranarauat

et osaa ovissa käyä veräjissä vieretellä
talon tyttären tavalla et tunne puhua tulta
etkä liettä lämmitteä talon miehen mieltä myöten

niinkö luulit neito nuori niinkö tiesit jotta luulit
luulit yöksi lähteväsi päivällä paloavasi
etpä yöksi lähtenynnä etkä yöksi et kaheksi
jopa jou'uit viikommaksi kuuksi päiväksi katosit
iäksi ison majoilta elinajaksi emosi
askelt' on piha pitempi kynnys hirttä korkeampi
sinun toiste tullessasi kerran kertaellessasi

neito parka huokaeli huokaeli henkäeli
suru syämelle panihe vesi silmille vetihe
itse tuon sanoiksi saatti noinpa tiesin noinpa luulin
noinpa arvelin ikäni sanoin kaiken kasvinaian
et sä neiti neiti olle oman vanhemman varassa
oman taaton tanterilla vanhan maammosi majoilla
äskenpä olisit neiti miehelähän mennessäsi
kuin oisi jalka kynnyksellä toinen korjassa kosijan
oisit päätäsi pitempi korvallista korkeampi

tuota toivoin tuon ikäni katsoin kaiken kasvinaian
vuotin kuin hyveä vuotta katsoin kuin kesän tuloa
jo nyt on toivoni toeksi lähtöni lähemmä saanut
jop' on jalka kynnyksellä toinen korjassa kosijan
enkä tuota tunnekana mikä muutti multa mielen
en lähe ilolla mielin enkä riemulla eriä
tästä kullasta ko'ista iän nuoren istumasta
näiltä kasvinkartanoilta ison saamilta eloilta
lähen hoikka huolissani ikävissäni eriän
kuin syksyisen yön sylihin kevä'isen kierän päälle
jälen jäällä tuntumatta jalan iskun iljangolla

miten lieki mieli muien mieli muien morsianten
tok' ei muut muretta tunne kanna kaihoista syäntä
kuin kannan minä katala kannan mustoa muretta
syäntä syen näköistä huolta hiilenkarvallista

niin on mieli miekkoisien autuaallisten ajatus
kuin keväinen päivännousu kevätaamun aurinkoinen
mitenpä minunki mieli minun synkeä sisuni
on kuin laaka lammin ranta kuin pimeä pilven ranta
kuin syksyinen yö pimeä talvinen on päivä musta
viel' on mustempi sitäki synkeämpi syksy-yötä

olipa akka askarvaimo talon ainoinen asuja
hänpä tuon sanoiksi virkki kutti kutti neiti nuori
etkö muista kuin sanelin sanelin saoinki kerroin
elä sulho'on ihastu elä sulhon suumalohon
luota silmänluontehesen katso jalkoihin jaloihin
sulovasti suun pitävi silmät luopi luopuisasti
vaikka lempo leukaluissa surma suussansa asuisi

noinpa aina neittä neuvoin orpanaistani opastin
kun tulevi suuret sulhot suuret sulhot maan kosijat
sinä vastahan sanele ja puhele puoleltasi
sanele sanalla tuolla lausu tuolla lausehella
'ei minusta ollekana ollekana lienekänä
miniäksi vietävätä orjaksi otettavata
ei neiti minun näköinen osaa orjana eleä
muista ei mukihin mennä olla aina alla kynsin
toinen kun sanan sanoisi minä kaksin vastoaisin
kun tulisi tukkahani hairahtaisi hapsihini
tukastani tuivertaisin hapsistani haivertaisin'

et sinä sitä totellut et kuullut minun sanoa
käeten kävit tulehen tieten tervan keittehesen
riensihit revon rekehen läksit karhun kantasille
revon reessänsä veteä karhun kauas kannatella
ikiorjaksi isännän aikaorjaksi anopin

läksit kouluhun kotoa piinahan ison pihoilta
kova on koulu käyäksesi piina pitkä ollaksesi
siell' on ohjat ostettuna varustettu vankirauat
ei ketänä muuta vasten vasten on vaivaista sinua

kohta saat kokea koito kokea kovaosainen
apen luista leukaluuta anopin kivistä kieltä
ky'yn kylmiä sanoja naon niskan-nakkeloita

kuules neiti kuin sanelen kuin sanelen kuin puhelen
olit kukkana kotona ilona ison pihoilla
iso kutsui kuutamaksi emo päivänpaisteheksi
veikkosi vesivaloksi siskosi siniveraksi
menet toisehen talohon vierahan emän alahan
ei vieras emosen verta vaimo toinen tuojan verta
harvoin vieras siivoin sinkui harvoin oike'in opetti
appi haukkuvi havuiksi anoppisi ahkioksi
kyty kynnysportahiksi nato naisien pahoiksi

äsken sie hyvä olisit äsken kerta kelpoaisit
utuna ulos menisit savuna pihalle saisit
lehtisenä lenteleisit kipunoina kiiättäisit

et ole lintu lentäjäksi etkä lehti liehujaksi
et kipuna kiitäjäksi savu saajaksi pihalle

voi neiti sisarueni jo nyt vaihoit minkä vaihoit
vaihoit armahan isosi appehen ani paha'an
vaihoit ehtoisen emosi anoppihin ankarahan
vaihoit viljon veljyesi kyyttäniskahan kytyhyn
vaihoit siskosi siveän naljasilmähän natohon
vaihoit liinavuotehesi nokisihin nuotioihin
vaihoit valkeat vetesi likaisihin lietehisin
vaihoit hiekkarantasesi mustihin muraperihin
vaihoit armahat ahosi kanervikkokankahisin
vaihoit marjaiset mäkesi kaskikantoihin kovihin

niinkö luulit neito nuori niinkö kasvava kananen
huolet loppui työt väheni tämän illan istumilla
maata sinne vietäväsi unille otettavasi

eip' on maata vieäkänä unille otetakana
vasta valvoa pitävi vasta huolta hoivatahan
ajatusta annetahan pannahan pahoa mieltä

kunis huiskit hunnutoinna sinis huiskit huoletoinna
kunis liikuit liinatoinna liikuit liioitta suruitta
äsken huntu huolta tuopi palttina pahoa mieltä
liina liikoja suruja pellava perättömiä

mikäs neitosen kotona niin neito ison kotona
kuin kuningas linnassansa yhtä miekkoa vajoa
toisin tuon miniä raukan niin miniä miehelässä
kuin vanki venäehellä yhtä vahtia vajoa

teki työtä työn ajalla väänti hartian väellä
hipiä hi'en väessä otsa vaahen valkeassa
kun tulevi toinen aika niin tulehen tuomitahan
ajetahan ahjoksehen sen kätehen käsketähän

piteä hänen pitäisi piteä piloisen piian
lohen mieli kiiskin kieli lammin ahvenen ajatus
suu sären salakan vatsa meriteiren tieto saa'a

eipä tieä yksikänä ymmärrä yheksänkänä
emon tuomista tytöistä vanhempansa vaalimista
mistä syöjä syntynevi kaluaja kasvanevi
lihan syöjä luun purija tukan tuulelle jakaja
hapsien hajottelija ahavalle anneksija

itke itke neiti nuori kun itket hyvinkin itke
itke kourin kyynelesi kahmaloin haluvetesi
pisaret ison pihoille lammit taaton lattioille
itke tulville tupanen siltalauat lainehille
kun et itke itkettäissä itket toiste tullessasi
kun tulet ison kotihin kun löyät isosi vanhan
saunahan savuttunehen kuiva vasta kainalossa

itke itke neiti nuori kun itket hyvinkin itke
kun et itke itkettäissä itket toiste tullessasi
kun tulet emon kotihin kun löyät emosi vanhan
läävähän läkähtynehen kuollehen kupo sylihin

itke itke neiti nuori kun itket hyvinkin itke
kun et itke itkettäissä itket toiste tullessasi
kun tulet tähän kotihin löyät veikkosi verevän
kujahan kukistunehen kartanolle kaatunehen

itke itke neiti nuori kun itket hyvinkin itke
kun et itke itkettäissä itket toiste tullessasi
kun tulet tähän talohon löyät siskosi siveän
sotkutielle sortunehen vanha karttu kainalossa

neito parka huokaeli huokaeli henkäeli
itse loihen itkemähän vierähti vetistämähän

itki kourin kyyneleitä kahmaloin haluvesiä
ison pestyille pihoille lammit taaton lattialle
siitä tuon sanoiksi virkki itse lausui ja pakisi
hoi sisaret sirkkuseni entiset ikätoverit
kaikki kasvinkumppalini kuulkottenpa kuin sanelen
en nyt tuota tunnekana mikä lienehe minulle
iskennä tämän ikävän tämän huolen hoivannunna
tämän kaihon kantanunna murehen mukaellunna

toisin tiesin toisin luulin toisin toivotin ikäni
käkesin käkenä käyä kukahella kukkuroilla
näille päivin päästyäni näille tuumin tultuani
enpä nyt käkenä käyne kukahelle kukkuroilla
olen kuin alli aallokossa tavi laajalla lahella
uiessa vilua vettä vettä jäistä järkyttäissä

voi isoni voi emoni voi on valtavanhempani
minnekä minua loitte kunne kannoitte katalan
nämät itkut itkemähän nämät kaihot kantamahan
nämät huolet huolimahan ja surut sureksimahan

mahoit ennen maammo rukka mahoit kaunis
kantajani
armas maion-antajani ihana imettäjäni
kapaloia kantosia pestä pieniä kiviä
kuin pesit tätä tytärtä kapalojit kaunoistasi
näille suurille suruille ape'ille miel'aloille

moni muualla sanovi usea ajattelevi
ei ole huolta hurnakolla ajatusta aioinkana
elkätte hyvät imeiset elkätte sitä sanoko
enemp' on minulla huolta kuin on koskessa kiviä
pajuja pahalla maalla kanervia kankahalla
hepo ei jaksaisi veteä rautakisko kingotella
ilman luokin lekkumatta vempelen värisemättä
noita hoikan huoliani mustia mure'itani

lauloi lapsi lattialta kasvavainen karsinasta
mitä neien itkemistä suuresti sureksimista
anna huolia hevosen murehtia mustan ruunan
rautasuisen surkutella suuripäisen päivitellä
hevosell' on pää parempi pää parempi luu lujempi
kaulan kaari kantavampi koko ruumis runsahampi

ei ole itettäviä suuresti surettavia
ei sinua suolle vieä ojavarrellen oteta
vievät viljamättähältä vievät vielä viljemmälle
ottavat oluttuvilta ottavat oluemmille

kun katsot kupehellesi oikealle puolellesi
onpa sulho suojassasi mies verevä vieressäsi
hyvä mies hyvä hevonen talon kanta kaikenlainen
pyyhyet pyräjämässä vempelellä vieremässä
rastahat iloitsemassa rahkehilla laulamassa
kuusi kullaista käkeä länkilöillä lekkumassa
seitsemän siniotusta reen kokalla kukkumassa

ellös olko milläkänä emon tuoma tuollakana
et panna pahenemahan pannahan paranemahan
miehen kyntäjän kylelle vakoajan vaipan alle
leivän saajan leuan alle kalan saajan kainalohon
hirven hiihtäjän hikehen karhun saajan saunasehen

miehen sait mitä jaloimman urohia uhke'imman
ei sen jouset jouten olle viinet vaarnoilla venyne
koirat ei ne koissa maanne pennut pehkuilla levänne

kolmasti tänä keväinä aivan aika-huomenessa
nousi nuotiotulelta havasi havusijalta
kolmasti tänä keväinä kaste on silmille karisnut
havut päänsä harjaellut varvat vartalon sukinut

mies on joukon jou'uttaja uros karjan kasvattaja
onpa tällä sulhollamme korvet koivin kulkevia
särkät säärin juoksevia noropohjan noutavia
sata on sarven kantajata tuhat tuojoa utaren
aumoja joka aholla purnuja joka purolla
lepikköiset leipämaina ojavieret ohramaina
karivieret kauramaina vesivieret vehnämaina
kaikki rauniot rahoina kivet pienet penninkinä
